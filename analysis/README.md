# Analysis software

This package contains the analysis of the reconstructed data.

For instructions on how to use the code using your CERN account on lxplus, check out the [mpgd-analysis user guide](https://mpgd-analysis.docs.cern.ch/guide/user/).

To install the analysis package on your private machine and contribute to the code development, also read the [developer guide](https://mpgd-analysis.docs.cern.ch/guide/compile/).