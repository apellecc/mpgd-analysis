import logging
import os

import awkward as ak
import numpy as np
import pandas as pd
import uproot

import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import mplhep as hep

plt.style.use(hep.style.ROOT)
#plt.rcParams.update({"font.size": 32})
#plt.rcParams.update({"image.cmap": "Purples"})
logging.getLogger('matplotlib.font_manager').disabled = True

from mpgd_analysis.qc8 import tools as qc8_tools
from mpgd_analysis.common import tools as common_tools
from mpgd_analysis.common import geometry 

def to_cartesian(a1, a2):
    a = ak.unzip(ak.cartesian((a1, a2)))
    return ak.flatten(a[0]).to_numpy(), ak.flatten(a[1]).to_numpy()

def analyze(ifile, odir, chosen_chambers, nevents, verbose):

    os.makedirs(odir, exist_ok=True)

    if verbose: logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    else: logging.basicConfig(level=logging.INFO, format="%(message)s")

    """ Load xml geometry files """
    setup = geometry.SetupGeometry("hcal-aug2023")

    with uproot.open(ifile) as rechit_file:

        rechit_tree = rechit_file["rechitTree"]
        events = rechit_tree.arrays(
            [
            "rechitChamber", "rechitEta",
            "rechitCharge", "rechitTime",
            "rechitX", "rechitY", "rechitClusterSize",
            ], entry_stop=nevents
        )

        rechits_chamber = events["rechitChamber"]
        rechits_eta = events["rechitEta"]
        rechits_cluster_size = events["rechitClusterSize"]
        rechits_charge = events["rechitCharge"]
        rechits_time = events["rechitTime"]
        rechits_x = events["rechitX"]
        rechits_y = events["rechitY"]

    for chamber_x, chamber_y in zip(*chosen_chambers):

        logging.info("Processing chambers {} and {}".format(chamber_x, chamber_y))

        charge_fig, charge_axs = plt.subplots(ncols=2, nrows=1, figsize=(11*2,9))
        charge2d_fig, charge2d_ax = plt.subplots(figsize=(11,9))
        charge_ratio_fig, charge_ratio_ax = plt.subplots(figsize=(11,9))
        time2d_fig, time_ax = plt.subplots(figsize=(11,9))
        cluster_size2d_fig, cluster_size2d_ax = plt.subplots(figsize=(11,9))

        charge_x = rechits_charge[rechits_chamber==chamber_x]
        charge_y = rechits_charge[rechits_chamber==chamber_y]
        time_x = rechits_time[rechits_chamber==chamber_x]
        time_y = rechits_time[rechits_chamber==chamber_y]
        cluster_size_x = rechits_cluster_size[rechits_chamber==chamber_x]
        cluster_size_y = rechits_cluster_size[rechits_chamber==chamber_y]

        """ Combine elements in pairs for histogram: """
        charge_x_broadcast, charge_y_broadcast = to_cartesian(charge_x, charge_y)
        time_x_broadcast, time_y_broadcast = to_cartesian(time_x, time_y)
        cluster_size_x_broadcast, cluster_size_y_broadcast = to_cartesian(cluster_size_x, cluster_size_y)

        charge_range = ((0,3500),(0,8500))
        for i,c in enumerate([charge_x, charge_y]):
            charge_axs[i].hist(ak.flatten(c), bins=200, range=charge_range[i], label="All charges", histtype="step", linewidth=2)
            charge_axs[i].hist(ak.max(c, axis=1), bins=200, range=charge_range[i], label="Max charge", histtype="step", linewidth=2)
            charge_axs[i].set_yscale("log")
            charge_axs[i].grid(which="both")
            charge_axs[i].legend()
            charge_axs[i].set_xlabel("Charge (ADC)")
            charge_axs[i].set_title("Detector {}".format([chamber_x, chamber_y][i]))

        charge_x_max, charge_y_max = ak.max(charge_x, axis=1).to_numpy(), ak.max(charge_y, axis=1).to_numpy()
        #charge2d_ax.hist2d(charge_x_broadcast, charge_y_broadcast, bins=200, range=charge_range, cmap="Blues")
        charge2d_ax.hist2d(charge_x_max, charge_y_max, bins=200, range=charge_range, cmap="Blues")
        charge2d_ax.set_xlabel("Charge x (ADC)")
        charge2d_ax.set_ylabel("Charge y (ADC)")

        charge_ratio_ax.hist(charge_y_max/charge_x_max, bins=200, range=(0,50), histtype="step", linewidth=2)
        charge_ratio_ax.set_xlabel("Charge y / charge x")
        charge_ratio_ax.set_xscale("log")
        charge_ratio_ax.set_yscale("log")
        charge_ratio_ax.grid(which="both")
        charge_ratio_ax.set_title("Detectors {} and {}".format(chamber_x, chamber_y))

        time_ax.hist2d(time_x_broadcast, time_y_broadcast, bins=10, range=((0,20),(0,20)), cmap="Blues")
        cluster_size2d_ax.hist2d(cluster_size_x_broadcast, cluster_size_y_broadcast, bins=8, range=((0.5,8.5),(0.5,8.5)), cmap="Blues")

        time_ax.set_xlabel("Arrival time x (BX)")
        time_ax.set_ylabel("Arrival time y (BX)")
        cluster_size2d_ax.set_xlabel("Cluster size x")
        cluster_size2d_ax.set_ylabel("Cluster size y")

        charge_ofile = odir / "charge-chambers-{}-{}.png".format(chamber_x, chamber_y)
        logging.info("    Saving charge plot to {}".format(charge_ofile))
        charge_fig.savefig(charge_ofile)

        charge2d_ofile = odir / "charge2d-chambers-{}-{}.png".format(chamber_x, chamber_y)
        logging.info("    Saving charge plot to {}".format(charge2d_ofile))
        charge2d_fig.savefig(charge2d_ofile)

        charge_ratio_ofile = odir / "charge-ratio-chambers-{}-{}.png".format(chamber_x, chamber_y)
        logging.info("    Saving charge ratio plot to {}".format(charge_ratio_ofile))
        charge_ratio_fig.savefig(charge_ratio_ofile)

        time_ofile = odir / "time-chambers-{}-{}.png".format(chamber_x, chamber_y)
        logging.info("    Saving time plot to {}".format(time_ofile))
        time2d_fig.savefig(time_ofile)

        cluster_size_ofile = odir / "clustersize-chambers-{}-{}.png".format(chamber_x, chamber_y)
        logging.info("    Saving cluster size plot to {}".format(cluster_size_ofile))
        cluster_size2d_fig.savefig(cluster_size_ofile)

