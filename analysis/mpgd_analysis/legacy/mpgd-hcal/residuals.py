import os, sys, pathlib
import argparse
import logging
from tqdm import tqdm

import uproot
import numpy as np
import pandas as pd
import awkward as ak
import scipy
from scipy.optimize import curve_fit
from scipy.stats import binned_statistic

import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import mplhep as hep
plt.style.use(hep.style.ROOT)
plt.rcParams.update({"font.size": 32})
#plt.rcParams.update({"image.cmap": "Purples"})
logging.getLogger('matplotlib.font_manager').disabled = True
hep.cms.label()#, data=<True|False>, lumi=50, year=2017)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("ifile", type=pathlib.Path, help="Input file")
    parser.add_argument('odir', type=pathlib.Path, help="Output directory")
    parser.add_argument("-n", "--events", type=int, default=-1, help="Number of events to analyse")
    parser.add_argument("--display", type=int, help="Number of events to display")
    parser.add_argument("-v", "--verbose", action="store_true", help="Activate logging")
    args = parser.parse_args()
    
    os.makedirs(args.odir, exist_ok=True)
    if args.verbose: logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    else: logging.basicConfig(level=logging.INFO, format="%(message)s")

    with uproot.open(args.ifile) as track_file:
        track_tree = track_file["trackTree"]
        if args.verbose: track_tree.show()

        logging.debug("Reading tree...")
        events = track_tree.arrays([
            "rechitChamber", "rechitGlobalX", "rechitGlobalY", "rechitCharge", "rechitTime",
            "prophitChamber", "prophitGlobalX", "prophitGlobalY",
            "trackSlopeX", "trackSlopeY", "trackInterceptX", "trackInterceptY"
            ], entry_stop=args.events)

        """ Assign hit variables """
        rechit_chamber = events.rechitChamber
        rechit_x = events.rechitGlobalX
        rechit_y = events.rechitGlobalY
        rechit_charge = events.rechitCharge
        rechit_time = events.rechitTime

        slope_x = events.trackSlopeX
        slope_y = events.trackSlopeY
        intercept_x = events.trackInterceptX
        intercept_y = events.trackInterceptY

        prophit_chamber = events.prophitChamber
        prophit_x = events.prophitGlobalX
        prophit_y = events.prophitGlobalY

        all_chambers = np.unique(ak.flatten(rechit_chamber))
        pad_chambers = np.unique(ak.flatten(prophit_chamber))
        tracking_chambers = set(all_chambers) - set(pad_chambers)

        logging.debug("All chambers: {}".format(all_chambers))
        logging.debug("Pad chambers: {}".format(pad_chambers))
        logging.debug("Tracking chambers: {}".format(tracking_chambers))

        """ Display first events """
        if args.display:

            os.makedirs(args.odir / "display", exist_ok=True)
            logging.info("Displaying first {} events...".format(args.display))

            nrows, ncols = 1, 2
            z_positions = np.array([ 88.5, 82, 73.5, 67, 59.5, 52, 161, 161.0001, 128.0, 128.0001 ])
            z_linspace = np.linspace(z_positions.min(), z_positions.max(), 10)

            for ievent in tqdm(range(args.display)):
                event_fig, event_axs = plt.subplots(nrows=nrows, ncols=ncols, figsize=(ncols*11, nrows*9))
                
                chamber_x_filter = [ ch in np.append(pad_chambers, [6, 8]) for ch in rechit_chamber[ievent] ]
                x = rechit_x[ievent][chamber_x_filter]
                ch = rechit_chamber[ievent][chamber_x_filter]
                m_x, q_x = slope_x[ievent], intercept_x[ievent]
                z = z_positions[ch]
                charge = rechit_charge[ievent][chamber_x_filter]
                
                event_im = event_axs[0].scatter(z, x, c = charge)
                event_axs[0].plot(z_linspace, q_x + m_x*z_linspace, color="red")
                event_axs[0].set_xlabel("z (mm)")
                event_axs[0].set_ylabel("x (mm)")
                event_axs[0].set_ylim(-100, 100)
                
                divider = make_axes_locatable(event_axs[0])
                cax = divider.append_axes("right", size="5%", pad=0.05)
                cax.set_ylabel("Charge (ADC)")
                event_fig.colorbar(event_im, cax=cax, orientation="vertical")
                
                """ Plot in y """
                chamber_y_filter = [ ch in np.append(pad_chambers, [7, 9]) for ch in rechit_chamber[ievent] ]
                y = rechit_y[ievent][chamber_y_filter]
                ch = rechit_chamber[ievent][chamber_y_filter]
                m_y, q_y = slope_y[ievent], intercept_y[ievent]
                z = z_positions[ch]
                charge = rechit_charge[ievent][chamber_y_filter]
                
                event_im = event_axs[1].scatter(z, y, c = charge)
                event_axs[1].plot(z_linspace, q_y + m_y*z_linspace, color="red")
                event_axs[1].set_xlabel("z (mm)")
                event_axs[1].set_ylabel("y (mm)")
                event_axs[1].set_ylim(-100, 100)
                
                divider = make_axes_locatable(event_axs[1])
                cax = divider.append_axes("right", size="5%", pad=0.05)
                cax.set_ylabel("Charge (ADC)")
                event_fig.colorbar(event_im, cax=cax, orientation="vertical")
            
                if abs(m_x) > 0.1 or abs(m_y) > 0.1:
                    continue

                event_fig.suptitle("Event {}".format(ievent))    
                event_fig.tight_layout()
                event_fig.savefig(args.odir / "display/{:03d}.png".format(ievent))

        """ Plot residuals and correlations """
        ncols = len(pad_chambers)
        nrows = 7
        residual_min_fig, residual_min_axs = plt.subplots(nrows=nrows, ncols=ncols, figsize=(11*ncols,9*nrows))
        residual_flat_fig, residual_flat_axs = plt.subplots(nrows=nrows, ncols=ncols, figsize=(11*ncols,9*nrows))
        residuals_range=(-100,100)

        for i_chamber,pad_chamber in enumerate(pad_chambers):
            
            filter_rechit = rechit_chamber==pad_chamber
            rec_x = rechit_x[filter_rechit]
            rec_y = rechit_y[filter_rechit]
            filter_prophit = prophit_chamber==pad_chamber
            prop_x = prophit_x[filter_prophit]
            prop_y = prophit_y[filter_prophit]
            
            """ Require a prophit in the event """
            track_exists = ak.count(prop_x, axis=1)>0
            rec_x, rec_y = rec_x[track_exists], rec_y[track_exists]
            prop_x, prop_y = ak.flatten(prop_x[track_exists]), ak.flatten(prop_y[track_exists])
            
            rec_x, prop_x = ak.broadcast_arrays(rec_x, prop_x)
            rec_y, prop_y = ak.broadcast_arrays(rec_y, prop_y)
            
            residual_x = rec_x - prop_x
            residual_y = rec_y - prop_y
            residual_r = np.sqrt(residual_x**2 + residual_y**2)
            
            logging.debug("Chamber {}".format(pad_chamber))
            logging.debug("  Rechit x: {}, size: {}".format(rec_x, ak.num(rec_x, axis=0)))
            logging.debug("  Rechit y: {}, size: {}".format(rec_y, ak.num(rec_y, axis=0)))
            logging.debug("  Prophit x: {}, size: {}".format(prop_x, ak.num(prop_x, axis=0)))
            logging.debug("  Prophit y: {}, size: {}".format(prop_y, ak.num(prop_y, axis=0)))
            
            residual_min_x = residual_x[ak.argmin(abs(residual_x), axis=1, keepdims=True)]
            residual_min_y = residual_y[ak.argmin(abs(residual_y), axis=1, keepdims=True)]
            residual_min_r = residual_r[ak.argmin(abs(residual_r), axis=1, keepdims=True)]
            
            residual_min_axs[0][i_chamber].hist(
                residual_min_x, bins=50, range=residuals_range,
                histtype="step", linewidth=2
            )
            residual_min_axs[0][i_chamber].grid()
            residual_min_axs[0][i_chamber].set_xlabel("Residual x (mm)")
            residual_min_axs[0][i_chamber].set_title(f"Pad chamber {pad_chamber}")

            residual_flat_axs[0][i_chamber].hist(
                ak.flatten(residual_x), bins=50, range=residuals_range,
                histtype="step", linewidth=2
            )
            residual_flat_axs[0][i_chamber].grid()
            residual_flat_axs[0][i_chamber].set_xlabel("Residual x (mm)")
            residual_flat_axs[0][i_chamber].set_title(f"Pad chamber {pad_chamber}")

            residual_min_axs[1][i_chamber].hist(
                residual_min_y, bins=50, range=residuals_range,
                histtype="step", linewidth=2
            )
            residual_min_axs[1][i_chamber].grid()
            residual_min_axs[1][i_chamber].set_xlabel("Residual y (mm)")
            residual_min_axs[1][i_chamber].set_title(f"Pad chamber {pad_chamber}")

            residual_flat_axs[1][i_chamber].hist(
                ak.flatten(residual_y), bins=50, range=residuals_range,
                histtype="step", linewidth=2
            )
            residual_flat_axs[1][i_chamber].grid()
            residual_flat_axs[1][i_chamber].set_xlabel("Residual y (mm)")
            residual_flat_axs[1][i_chamber].set_title(f"Pad chamber {pad_chamber}")

            residual_min_axs[2][i_chamber].hist2d(
                ak.min(prop_x, axis=1).to_numpy(),
                ak.flatten(prop_x).to_numpy(),
                ak.flatten(rec_x).to_numpy(),
                bins=100, range=((-100,100),(-100,100)),
            )
            residual_min_axs[2][i_chamber].set_xlabel("Prophit x (mm)")
            residual_min_axs[2][i_chamber].set_ylabel("Rechit x (mm)")

            residual_flat_axs[2][i_chamber].hist2d(
                ak.flatten(prop_x).to_numpy(),
                ak.flatten(rec_x).to_numpy(),
                bins=100, range=((-100,100),(-100,100)),
            )
            residual_flat_axs[2][i_chamber].set_xlabel("Prophit x (mm)")
            residual_flat_axs[2][i_chamber].set_ylabel("Rechit x (mm)")

            residual_min_axs[3][i_chamber].hist2d(
                ak.flatten(prop_x).to_numpy(),
                ak.flatten(residual_x).to_numpy(),
                bins=100, range=((-100,100),residuals_range),
            )
            residual_min_axs[3][i_chamber].set_xlabel("Prophit x (mm)")
            residual_min_axs[3][i_chamber].set_ylabel("Residual x (mm)")
            
            residual_min_axs[4][i_chamber].hist2d(
                ak.flatten(prop_y).to_numpy(),
                ak.flatten(rec_y).to_numpy(),
                bins=100, range=((-100,100),(-100,100)),
            )
            residual_min_axs[4][i_chamber].set_xlabel("Prophit y (mm)")
            residual_min_axs[4][i_chamber].set_ylabel("Rechit y (mm)")
            
            residual_min_axs[5][i_chamber].hist2d(
                ak.flatten(prop_y).to_numpy(),
                ak.flatten(residual_y).to_numpy(),
                bins=100, range=((-100,100),residuals_range),
            )
            residual_min_axs[5][i_chamber].set_xlabel("Prophit y (mm)")
            residual_min_axs[5][i_chamber].set_ylabel("Residual y (mm)")

            residual_min_axs[6][i_chamber].hist2d(
                ak.min(prop_x, axis=1).to_numpy(),
                ak.flatten(residual_min_x),
                bins=100, range=((-100,100),residuals_range),
            )
            residual_min_axs[6][i_chamber].set_xlabel("Prophit x (mm)")
            residual_min_axs[6][i_chamber].set_ylabel("Residual x min (mm)")
             
        residual_min_fig.tight_layout()
        residual_min_fig.savefig(args.odir / "residuals.png")

if __name__=='__main__': main()
