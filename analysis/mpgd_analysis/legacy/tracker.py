import os, sys, pathlib
import argparse
import logging
from tqdm import tqdm

import uproot
import numpy as np
import pandas as pd
import awkward as ak
import scipy
from scipy.optimize import curve_fit
from scipy.stats import binned_statistic

import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import mplhep as hep
plt.style.use(hep.style.ROOT)
plt.rcParams.update({"font.size": 32})
plt.rcParams.update({"image.cmap": "Purples"})
logging.getLogger('matplotlib.font_manager').disabled = True
hep.cms.label()#, data=<True|False>, lumi=50, year=2017)

from concurrent.futures import ThreadPoolExecutor
executor = ThreadPoolExecutor(8)

chamber_text = "Tracker {}\n"\
        +"10x10 $cm^2$ triple-GEM\n"\
        +"250 µm strip pitch\n"\
        +"Ar-$CO_2$ 70%-30%\n"\
        +"$5\,M#Omega$ high voltage divider\n"\

def linear_function(x, *p):
    q, m = p
    return q + m*x

def gauss(x, *p):
    A, mu, sigma = p
    return A*scipy.stats.norm.pdf(x, loc=mu, scale=sigma)
    #return A*np.exp(-(x-mu)**2/(2.*sigma**2))

def gauss2(x, *p):
    A1, mu1, sigma1, A2, mu2, sigma2 = p
    # to be checked
    return A1*scipy.stats.norm.pdf(x, loc=mu1, scale=sigma1) + A2*scipy.stats.norm.pdf(x, loc=mu2, scale=sigma2)
    #return gauss(x, A1, mu1, sigma1) + gauss(x, A2, mu2, sigma2)

def analyse_residuals(residuals, histo_range, nbins, ax, color="red", func=gauss):
   
    points, edges = np.histogram(residuals, bins=nbins, range=histo_range)
    bins = 0.5*(edges[1:]+edges[:-1])
    
    # gaussian fit
    coeff = [len(residuals), ak.mean(residuals), ak.std(residuals)]
    if func==gauss2: coeff += [len(residuals)*0.1, ak.mean(residuals), 10*ak.std(residuals)]
    perr = [0]*len(coeff)
    try:
        coeff, var_matrix = curve_fit(func, bins, points, p0=coeff)#, method="lm")
        perr = np.sqrt(np.diag(var_matrix))
    except RuntimeError:
        logging.error("Fit failed, using RMS instead...")
    
    correction, err_correction = coeff[1], perr[1]
    space_resolution, err_space_resolution = 1e3*coeff[2], 1e3*perr[2]
    
    # plot data and fit
    xvalues = np.linspace(bins[0], bins[-1], 1000)
    if ax:
        ax.plot(xvalues, func(xvalues, *coeff), color=color, linewidth=2)
        fit_text = f"$x_0$ {correction:1.2f} $\pm$ {err_correction:1.2f} mm\n"
        fit_text += f"$\sigma$ {space_resolution:1.1f} $\pm$ {err_space_resolution:1.1f} µm"
        ax.text(
            0.9, 0.9, fit_text,
            transform=ax.transAxes, ha="right", va="top"
        )

    return correction, err_correction, space_resolution, err_space_resolution

def analyse_rotation(prophits, rechits, bins, range, ax=None):

    # Calculate and plot residual bins for rotation
    residual_means, prophit_edges, prophit_indices = binned_statistic(
        prophits, rechits, statistic="mean", bins=bins, range=range
    )
    residual_errors, prophit_edges, prophit_indices = binned_statistic(
        prophits, rechits, statistic="std", bins=bins, range=range
    )
    prophit_bins = 0.5*(prophit_edges[1:]+prophit_edges[:-1])

    # Fit residual trend
    coeff = [
        0.5*(residual_means[0]+residual_means[-1]),
        (residual_means[-1]-residual_means[0])/(prophit_bins[-1]-prophit_bins[0])
    ]
    try:
        coeff, var_matrix = curve_fit(linear_function, prophit, rechits, p0=coeff)
        q, m = coeff
        err_q, err_m = np.sqrt(np.diag(var_matrix))
    except Exception as e:
        logging.error("Skipping rotation fit:", e)

    # Calculate angles:
    theta = np.arcsin(m)
    err_theta = err_m/np.sqrt(1-m**2)

    if ax:
        x_fit = np.linspace(prophit_bins[0], prophit_bins[-1], 5)
        ax.errorbar(prophit_bins, residual_means, yerr=residual_errors, fmt=".k")
        ax.plot(x_fit, linear_function(x_fit, *coeff), color="red")
        ax.text(
            0.95, 0.93,
            f"q = {q*1e3:1.2f} $\pm$ {err_q*1e3:1.1f} µm\n"+
            f"ϑ = {theta*1e3:1.1f} $\pm$ {err_theta*1e3:1.1f} mrad",
            transform=ax.transAxes,
            ha="right", va="top", fontsize=28, linespacing=1.5
        )
    return theta, err_theta

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("ifile", type=pathlib.Path, help="Input file")
    parser.add_argument('odir', type=pathlib.Path, help="Output directory")
    parser.add_argument("-n", "--events", type=int, default=-1, help="Number of events to analyse")
    parser.add_argument("--chambers-x", type=int, nargs="+", help="Detectors with x readout")
    parser.add_argument("--chambers-y", type=int, nargs="+", help="Detectors with y readout")
    parser.add_argument("--residual-range", type=float, nargs="+", default=[-3,3], help="Range of residual distribution")
    parser.add_argument("--by-cluster-size", action="store_true", help="Plot profile as 2D plot with cluster size")
    parser.add_argument("-v", "--verbose", action="store_true", help="Activate logging")
    args = parser.parse_args()
    
    os.makedirs(args.odir, exist_ok=True)
    if args.verbose: logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    else: logging.basicConfig(level=logging.INFO, format="%(message)s")

    with uproot.open(args.ifile) as track_file:
        track_tree = track_file["trackTree"]
        if args.verbose: track_tree.show()

        logging.debug("Reading tree...")
        events = track_tree.arrays([
            "rechitChamber", "rechitGlobalX", "rechitGlobalY",
            "partialTrackChamber", "partialTrackChi2",
            "partialProphitGlobalX", "partialProphitGlobalY",
            "rechitClusterSize"
            ], entry_stop=args.events)

        rechits_chamber = events["rechitChamber"]
        rechits_x = events["rechitGlobalX"]
        rechits_y = events["rechitGlobalY"]
        prophits_chamber = events["partialTrackChamber"]
        prophits_x = events["partialProphitGlobalX"]
        prophits_y = events["partialProphitGlobalY"]
        chi2 = events["partialTrackChi2"]

        """ Apply basic filters """
        chi2_filter = (chi2>=0)&(chi2<=1000000)
        prophits_chamber = prophits_chamber[chi2_filter]
        prophits_x = prophits_x[chi2_filter]
        prophits_y = prophits_y[chi2_filter]
        chi2 = chi2[chi2_filter]

        logging.debug("Rechit variables:")
        logging.debug("  Chambers: {}, size {}".format(rechits_chamber, ak.count(rechits_chamber, axis=1)))
        logging.debug("  x: {}, size {}".format(rechits_x, ak.count(rechits_x, axis=1)))
        logging.debug("  y: {}, size {}".format(rechits_y, ak.count(rechits_y, axis=1)))

        logging.debug("Prophit variables:")
        logging.debug("  Chambers: {}, size {}".format(prophits_chamber, ak.count(prophits_chamber, axis=1)))
        logging.debug("  x: {}, size {}".format(prophits_x, ak.count(prophits_x, axis=1)))
        logging.debug("  y: {}, size {}".format(prophits_y, ak.count(prophits_y, axis=1)))

        """ Define dictionaries for translation and rotation in x and y """
        translation = [dict(), dict()]
        err_translation = [dict(), dict()]
        rotation = dict()
        err_rotation = dict()

        """ Plot and fit space resolution and rotation for all detectors """
        chambers = args.chambers_x + args.chambers_y
        for tested_chamber in chambers:

            logging.info("Analysing chamber %d", tested_chamber)
            rechits_x_chamber = rechits_x[rechits_chamber==tested_chamber]
            rechits_y_chamber = rechits_y[rechits_chamber==tested_chamber]
            prophits_x_chamber = prophits_x[prophits_chamber==tested_chamber]
            prophits_y_chamber = prophits_y[prophits_chamber==tested_chamber]
            chi2_chamber = chi2[prophits_chamber==tested_chamber]

            logging.debug("  Rechits x: {}".format(rechits_x_chamber))
            logging.debug("  Rechits y: {}".format(rechits_y_chamber))
            logging.debug("  Prophits x: {}".format(prophits_x_chamber))
            logging.debug("  Prophits y: {}".format(prophits_y_chamber))

            """ Plot rechit and propagated occupancies """
            chi2_bins, chi2_range = 100, (-0.01,1000000)
            chi2_fig, chi2_ax = plt.subplots(nrows=1, ncols=1, figsize=(11,9))
            _, chi2_edges, _ = chi2_ax.hist(ak.flatten(chi2_chamber), bins=chi2_bins, range=chi2_range)
            chi2_binning = np.diff(chi2_edges).mean()

            chi2_ax.set_xlabel("Track $\chi^2$")
            chi2_ax.set_ylabel(f"Events / {chi2_binning:1.0f}")

            chi2_ofile = args.odir/f"chi2_chamber_{tested_chamber}.png"
            logging.info("  Saving chi2 to %s", chi2_ofile)
            chi2_fig.savefig(chi2_ofile)

            """ Plot rechit and propagated occupancies """
            occupancy_bins, occupancy_range = 100, (-50,50)
            occupancy_fig, occupancy_axs = plt.subplots(nrows=2, ncols=2, figsize=(22,18))
            _, occupancy_edges, _ = occupancy_axs[0][0].hist(ak.flatten(rechits_x_chamber), bins=occupancy_bins, range=occupancy_range)
            occupancy_axs[0][1].hist(ak.flatten(rechits_y_chamber), bins=occupancy_bins, range=occupancy_range)
            occupancy_axs[1][0].hist(ak.flatten(prophits_x_chamber), bins=occupancy_bins, range=occupancy_range)
            occupancy_axs[1][1].hist(ak.flatten(prophits_y_chamber), bins=occupancy_bins, range=occupancy_range)
            occupancy_binning = np.diff(occupancy_edges).mean()*1e3

            occupancy_axs[0][0].set_xlabel("Reconstructed x (mm)")
            occupancy_axs[0][1].set_xlabel("Reconstructed y (mm)")
            occupancy_axs[1][0].set_xlabel("Propagated x (mm)")
            occupancy_axs[1][1].set_xlabel("Propagated y (mm)")
            for ax in occupancy_axs.flat: ax.set_ylabel(f"Events / {occupancy_binning:1.0f} µm")

            occupancy_ofile = args.odir/f"occupancy_chamber_{tested_chamber}.png"
            logging.info("  Saving occupancy to %s", occupancy_ofile)
            occupancy_fig.tight_layout()
            occupancy_fig.savefig(occupancy_ofile)
            plt.close(occupancy_fig)

            """ Plot and fit partial track residuals """ 
            # choose only events with both a prophit and a rechit
            residual_x_filter = (ak.count(rechits_x_chamber, axis=1)>0)&(ak.count(prophits_x_chamber, axis=1)>0)
            residual_y_filter = (ak.count(rechits_y_chamber, axis=1)>0)&(ak.count(prophits_y_chamber, axis=1)>0)
            residuals_x = rechits_x_chamber[residual_x_filter]-prophits_x_chamber[residual_x_filter]
            residuals_y = rechits_y_chamber[residual_y_filter]-prophits_y_chamber[residual_y_filter]
            
            residual_bins, residual_range = 50, args.residual_range
            residual_fig, residual_axs = plt.subplots(nrows=1, ncols=2, figsize=(22,9))
            residual_settings = dict(
                    bins=residual_bins, range=residual_range, 
                    histtype="stepfilled", linewidth=2, facecolor="none", edgecolor="k"
                    )
            _, residual_edges, _ = residual_axs[0].hist(residuals_x, **residual_settings)
            residual_axs[1].hist(residuals_y, **residual_settings)
            residual_binning = np.diff(residual_edges).mean()*1e3

            residual_axs[0].set_xlabel("Residual x (mm)")
            residual_axs[1].set_xlabel("Residual y (mm)")
            for ax in residual_axs: ax.set_ylabel(f"Events / {residual_binning:1.0f} µm")

            # Fit residuals:
            if tested_chamber in args.chambers_x: direction = 0
            elif tested_chamber in args.chambers_y: direction = 1
            else: raise ValueError("Chamber {} is neither in x nor in y list".format(tested_chamber))
            
            correction, err_correction, space_resolution, err_space_resolution = analyse_residuals(
                    [residuals_x, residuals_y][direction],
                    residual_range, residual_bins,
                    ax=residual_axs[direction],
                    color=["red","blue"][direction]
                    )
            translation[direction][tested_chamber] = correction
            err_translation[direction][tested_chamber] = err_correction

            residual_ofile = args.odir/f"residual_chamber_{tested_chamber}.png"
            logging.info("  Saving residuals to %s", residual_ofile)
            residual_fig.tight_layout()
            residual_fig.savefig(residual_ofile)
            plt.close(residual_fig)

            """ Determine rotation corrections """
            rotation_bins, rotation_range = (50,50), ((-20,20),args.residual_range)
            rotation_fig, rotation_ax = plt.subplots(nrows=1, ncols=1, figsize=(11,9))
 
            # Plot 2D histogram for rotation
            p = [prophits_x_chamber,prophits_y_chamber][direction][residual_x_filter]
            r = [residuals_x,residuals_y][direction]
            rotation_mask = (p>rotation_range[0][0])&(p<rotation_range[0][1])
            rotation_mask = (rotation_mask)&(r>rotation_range[1][0])&(r<rotation_range[1][1])
            p, r = ak.flatten(p[rotation_mask]), ak.flatten(r[rotation_mask])
            #rotation_ax.plot(p, r, ".")
            rotation_ax.hist2d(p, r, bins=rotation_bins, range=rotation_range)
            
            # Plot and fit rotation corrections
            rotation[tested_chamber], err_rotation[tested_chamber] = analyse_rotation(p, r, bins=rotation_bins[0], range=rotation_range[0], ax=rotation_ax)
            #rotation[tested_chamber], err_rotation[tested_chamber] = 0, 0

            rotation_ofile = args.odir/f"rotation_chamber_{tested_chamber}.png"
            logging.info("  Saving rotation to %s", rotation_ofile)
            rotation_fig.tight_layout()
            rotation_fig.savefig(rotation_ofile)
            plt.close(rotation_fig)
 
        """ Combine x and y translation and angle corrections, then save: """
        corrections = {
            "translation_x": translation[0], "translation_y": translation[1],
            "err_translation_x": err_translation[0], "err_translation_y": err_translation[1],
            "angle": rotation, "err_angle": err_rotation
        }
        corrections_df = pd.DataFrame.from_dict(corrections).fillna(0).sort_index()
        logging.info("Corrections:")
        logging.info(corrections_df)
        correction_ofile = args.odir / "corrections.txt"
        corrections_df.to_csv(correction_ofile, sep=";", index_label="chamber")
        logging.info("Corrections saved to %s", correction_ofile)

if __name__=='__main__': main()
