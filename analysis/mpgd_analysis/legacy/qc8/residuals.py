import os, sys, pathlib
import argparse
import logging
from tqdm import tqdm

import uproot
import numpy as np
import pandas as pd
import awkward as ak
np.seterr(divide='ignore', invalid='ignore')

from scipy.optimize import curve_fit
from scipy.stats import binned_statistic
from scipy.stats import norm

import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import mplhep as hep

plt.style.use(hep.style.ROOT)
plt.rcParams.update({"font.size": 32})
plt.rcParams.update({"image.cmap": "Purples"})
logging.getLogger('matplotlib.font_manager').disabled = True
hep.cms.label()#, data=<True|False>, lumi=50, year=2017)

gauss_pdf = lambda x, mu, k, s: k * norm.pdf(x, mu, s)
gauss2_pdf = lambda x, m1, k1, s1, m2, k2, s2: gauss_pdf(x, m1, k1, s1) + gauss_pdf(x, m2, k2, s2)

def lin_interp(x, y, i, half):
    return x[i] + (x[i+1] - x[i]) * ((half - y[i]) / (y[i+1] - y[i]))

def fwhm(x, y):
    half = max(y)/2.0
    signs = np.sign(y-half)
    zero_crossings = (signs[0:-2] != signs[1:-1])
    zero_crossings_i = np.where(zero_crossings)[0]
    try:
        return [lin_interp(x, y, zero_crossings_i[0], half), lin_interp(x, y, zero_crossings_i[1], half)]
    except IndexError:
        return [-0.3, 0.3]

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("ifile", type=pathlib.Path, help="Input file")
    parser.add_argument('odir', type=pathlib.Path, help="Output directory")
    parser.add_argument("-n", "--events", type=int, default=-1, help="Number of events to analyse")
    parser.add_argument("--fwhm", action="store_true", help="Calculate FWHM instead of residual sigma")
    parser.add_argument("-v", "--verbose", action="store_true", help="Activate logging")
    args = parser.parse_args()
    
    os.makedirs(args.odir, exist_ok=True)
    if args.verbose: logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    else: logging.basicConfig(level=logging.INFO, format="%(message)s")

    with uproot.open(args.ifile) as track_file:
        track_tree = track_file["trackTree"]
        if args.verbose: track_tree.show()

        logging.info("Reading tree...")
        events = track_tree.arrays(
            [
            "rechitChamber", "rechitEta",
            "rechitLocalX", "rechitLocalY", "rechitClusterSize",
            "rechitR", "rechitPhi",
            "trackSlopeX", "trackSlopeY",
            "partialTrackChamber", "partialTrackChi2",
            "partialTrackSlopeX", "partialTrackSlopeY",
            "partialProphitGlobalX", "partialProphitGlobalY",
            "partialProphitEta"
            ], entry_stop=args.events
        )

        rechits_chamber = events["rechitChamber"]
        rechits_eta = events["rechitEta"]
        rechits_cluster_size = events["rechitClusterSize"]
        rechits_x = events["rechitLocalX"]
        rechits_y = events["rechitLocalY"]
        rechits_r = events["rechitR"]
        rechits_phi = events["rechitPhi"]

        track_slope_x = events["trackSlopeX"]
        track_slope_y = events["trackSlopeY"]

        prophits_chamber = events["partialTrackChamber"]
        prophits_chi2 = events["partialTrackChi2"]
        prophits_slope_x = events["partialTrackSlopeX"]
        prophits_slope_y = events["partialTrackSlopeY"]
        prophits_eta = events["partialProphitEta"]
        prophits_x = events["partialProphitGlobalX"]
        prophits_y = events["partialProphitGlobalY"]

        track_quality_filter = (prophits_chi2<100)&(prophits_chi2>0.001)
        track_quality_filter = track_quality_filter&(abs(prophits_slope_x)<1000e-3)
        track_quality_filter = track_quality_filter&(abs(prophits_slope_y)<0.01)
        prophits_chamber = prophits_chamber[track_quality_filter]
        prophits_chi2 = prophits_chi2[track_quality_filter]
        prophits_slope_x = prophits_slope_x[track_quality_filter]
        prophits_slope_y = prophits_slope_y[track_quality_filter]
        prophits_eta = prophits_eta[track_quality_filter]
        prophits_x = prophits_x[track_quality_filter]
        prophits_y = prophits_y[track_quality_filter]

        logging.debug("Rechit variables:")
        logging.debug("  Chambers: {}, size {}".format(rechits_chamber, ak.count(rechits_chamber, axis=1)))
        logging.debug("  x: {}, size {}".format(rechits_x, ak.count(rechits_x, axis=1)))
        logging.debug("  y: {}, size {}".format(rechits_y, ak.count(rechits_y, axis=1)))

        logging.debug("Prophit variables:")
        logging.debug("  Chambers: {}, size {}".format(prophits_chamber, ak.count(prophits_chamber, axis=1)))
        logging.debug("  x: {}, size {}".format(prophits_x, ak.count(prophits_x, axis=1)))
        logging.debug("  y: {}, size {}".format(prophits_y, ak.count(prophits_y, axis=1)))

        list_chambers = np.unique(ak.flatten(rechits_chamber))
        n_chambers = len(list_chambers)
        logging.info("{} chambers in setup".format(n_chambers))
        list_eta = np.unique(ak.values_astype(ak.flatten(rechits_eta), int))
        n_eta = len(list_eta)
        logging.info("{} eta partitions in setup".format(n_eta))

        """ Define figures """
        slopes_fig, slopes_axs = plt.subplots(nrows=1, ncols=2, figsize=(11*2,9))
        profile_fig, profile_axs = plt.subplots(nrows=2, ncols=n_chambers, figsize=(11*n_chambers,9*2))
        eta_fig, eta_axs = plt.subplots(nrows=2, ncols=n_chambers, figsize=(11*n_chambers,9*2))
        profile2d_fig, profile2d_axs = plt.subplots(nrows=2, ncols=n_chambers, figsize=(11*n_chambers,9*2))
        cluster_size_fig, cluster_size_axs = plt.subplots(ncols=n_chambers, nrows=n_eta, figsize=(11*n_chambers,9*n_eta))
        residuals_fig, residuals_axs = plt.subplots(nrows=n_eta, ncols=n_chambers, figsize=(11*n_chambers,9*n_eta))
        prophits_rechits_fig, prophits_rechits_axs = plt.subplots(nrows=n_eta, ncols=n_chambers, figsize=(11*n_chambers,9*n_eta))
        prophits_residuals_fig, prophits_residuals_axs = plt.subplots(nrows=n_eta, ncols=n_chambers, figsize=(11*n_chambers,9*n_eta))
        matching_fig, matching_axs = plt.subplots(nrows=n_eta, ncols=n_chambers, figsize=(11*n_chambers,9*n_eta))
        efficiency_profile_fig, efficiency_profile_axs = plt.subplots(nrows=n_eta, ncols=n_chambers, figsize=(11*n_chambers,9*n_eta))
        efficiency_scan_fig, efficiency_scan_axs = plt.subplots(nrows=n_eta, ncols=n_chambers, figsize=(11*n_chambers,9*n_eta))

        """ Plot track slopes """
        track_angle_x, track_angle_y = np.arctan(track_slope_x), np.arctan(track_slope_y)
        slopes_counts, slopes_edges, _ = slopes_axs[0].hist(track_angle_x*1e3, bins=100, range=(-1000, 1000), histtype="step", linewidth=2)
        slopes_centers = 0.5*(slopes_edges[1:]+slopes_edges[:-1])
        slopes_axs[1].hist(track_slope_y, bins=100, range=(-1000, 1000), histtype="step", linewidth=2)
        
        slopes_mean = np.mean(track_angle_x*1e3)
        slopes_hm = fwhm(slopes_centers, slopes_counts)
        slopes_fwhm = slopes_hm[1]-slopes_hm[0]
        slopes_axs[0].annotate(
            f"Mean {slopes_mean:1.2f}\nFWHM {slopes_fwhm:1.2f}",
            xy=(0.1,0.9), xycoords="axes fraction", va="top"
        )

        slopes_axs[0].set_xlabel("Track angle x (mrad)")
        slopes_axs[1].set_xlabel("Track angle y (mrad)")

        slopes_fig.tight_layout()
        logging.info("Saving track slopes to {}".format(args.odir / "track_slopes.pdf"))
        slopes_fig.savefig(args.odir / "track_slopes.png")
        slopes_fig.savefig(args.odir / "track_slopes.pdf")

        for ichamber,tested_chamber in enumerate(list_chambers):
            logging.info("Analyzing chamber {}...".format(tested_chamber))

            rechits_cluster_size_chamber = rechits_cluster_size[rechits_chamber==tested_chamber]
            rechits_eta_chamber = rechits_eta[rechits_chamber==tested_chamber]
            rechits_x_chamber = rechits_x[rechits_chamber==tested_chamber]
            rechits_y_chamber = rechits_y[rechits_chamber==tested_chamber]
            rechits_r_chamber = rechits_r[rechits_chamber==tested_chamber]
            rechits_phi_chamber = rechits_phi[rechits_chamber==tested_chamber]

            prophits_eta_chamber = prophits_eta[prophits_chamber==tested_chamber]
            prophits_x_chamber = prophits_x[prophits_chamber==tested_chamber]
            prophits_y_chamber = prophits_y[prophits_chamber==tested_chamber]

            """ Plot eta partition distributions """
            eta_axs[0][ichamber].hist(
                    ak.flatten(rechits_eta_chamber), 
                    bins=8, range=(0.5, 8.5),
                    histtype="step", color="blue", linewidth=2,
            )
            eta_axs[1][ichamber].scatter(
                    ak.flatten(rechits_eta_chamber), ak.flatten(rechits_y_chamber),
            )
            eta_axs[0][ichamber].set_xlabel("Rechit eta partition")
            eta_axs[1][ichamber].set_xlabel("Rechit eta partition")
            eta_axs[1][ichamber].set_ylabel("Rechit y (mm)")

            """ Plot 2D profiles """
            profile2d_axs[0][ichamber].hist2d(
                    ak.flatten(rechits_x_chamber).to_numpy(), ak.flatten(rechits_y_chamber).to_numpy(),
                    bins=(100,100), range=((-200,200),(-400,500)), cmap="Blues"
            )
            profile2d_axs[1][ichamber].hist2d(
                    ak.flatten(prophits_x_chamber).to_numpy(), ak.flatten(prophits_y_chamber).to_numpy(),
                    bins=(100,100), range=((-200,200),(-400,500)), cmap="Blues"
            )
            profile2d_axs[0][ichamber].set_xlabel("Rechit x (mm)")
            profile2d_axs[0][ichamber].set_ylabel("Rechit y (mm)")
            profile2d_axs[1][ichamber].set_xlabel("Prophit x (mm)")
            profile2d_axs[1][ichamber].set_ylabel("Prophit y (mm)")

            """ Plot 1D rechit and prophit profiles """
            profile_axs[0][ichamber].hist(
                    ak.flatten(rechits_x_chamber), 
                    bins=100, range=(-500,500),
                    histtype="step", color="red", linewidth=2,
                    label="Rechits"
            )
            profile_axs[1][ichamber].hist(
                    ak.flatten(rechits_y_chamber), 
                    bins=100, range=(-500,500),
                    histtype="step", color="red", linewidth=2,
                    label="Rechits"
            )
            profile_axs[0][ichamber].hist(
                    ak.flatten(prophits_x_chamber), 
                    bins=100, range=(-500,500),
                    histtype="step", color="blue", linewidth=2,
                    label="Prophits"
            )
            profile_axs[1][ichamber].hist(
                    ak.flatten(prophits_y_chamber), 
                    bins=100, range=(-500,500),
                    histtype="step", color="blue", linewidth=2,
                    label="Prophits"
            )
            profile_axs[0][ichamber].set_xlabel("x (mm)")
            profile_axs[1][ichamber].set_xlabel("y (mm)")
            profile_axs[0][ichamber].legend()
            profile_axs[1][ichamber].legend()

            for ieta,tested_eta in enumerate(list_eta):
                logging.info("    Analyzing eta {}...".format(tested_eta))
                eta_title = f"Detector {tested_chamber} eta {tested_eta}"

                filter_eta = rechits_eta_chamber==tested_eta
                rechits_cluster_size_eta = rechits_cluster_size_chamber[filter_eta]
                rechits_x_eta = rechits_x_chamber[filter_eta]
                rechits_y_eta = rechits_y_chamber[filter_eta]
                rechits_r_eta = rechits_r_chamber[filter_eta]
                rechits_phi_eta = rechits_phi_chamber[filter_eta]

                prophits_x_eta = prophits_x_chamber[prophits_eta_chamber==tested_eta]
                prophits_y_eta = prophits_y_chamber[prophits_eta_chamber==tested_eta]

                """ Plot cluster size profiles """
                cluster_size_axs[ieta][ichamber].hist(
                        ak.flatten(rechits_cluster_size_eta),
                        bins=11, range=(0.5, 11.5),
                        histtype="step", color="blue", linewidth=2
                )
                cluster_size_axs[ieta][ichamber].set_xlabel("Cluster size")
                cluster_size_axs[ieta][ichamber].set_ylabel("Counts")
                cluster_size_axs[ieta][ichamber].text(
                    0.9, 0.9, f"Detector {tested_chamber} eta {tested_eta}", 
                    transform=cluster_size_axs[ieta][ichamber].transAxes, ha="right"
                )
                cluster_size_axs[ieta][ichamber].set_title(eta_title)

                """ Process events for residuals """
                # choose only events with both a prophit and a rechit and broadcast the prophits
                residual_filter = (ak.count(rechits_x_eta, axis=1)>0)&(ak.count(prophits_x_eta, axis=1)>0)
                rechits_x_broadcast, prophits_x_broadcast = ak.broadcast_arrays(
                        rechits_x_eta[residual_filter],
                        ak.flatten(prophits_x_eta[residual_filter])
                )
                residual_filter = (ak.count(rechits_y_eta, axis=1)>0)&(ak.count(prophits_y_eta, axis=1)>0)
                rechits_y_broadcast, prophits_y_broadcast = ak.broadcast_arrays(
                        rechits_y_eta[residual_filter],
                        ak.flatten(prophits_y_eta[residual_filter])
                )

                #position_filter = abs(prophits_x_broadcast)>10
                residuals_y = rechits_y_broadcast-prophits_y_broadcast
                residuals_x = rechits_x_broadcast-prophits_x_broadcast
                #residuals_x_flat = residuals_x[ak.argmin(residuals_x, axis=1, keepdims=True)].to_numpy()
                residuals_x_flat = ak.flatten(residuals_x).to_numpy()
    
                """ Plot and fit residuals """
                counts, x_edges, _ = residuals_axs[ieta][ichamber].hist(
                    residuals_x_flat,
                    histtype="step", linewidth=2, color="blue",
                    bins=100, range=(-5,5)
                )
                x_centers = 0.5*(x_edges[1:]+x_edges[:-1])
                residuals_binning = np.diff(x_edges).mean()
                residuals_axs[ieta][ichamber].set_xlabel("All residual x (mm)")
                residuals_axs[ieta][ichamber].set_ylabel(f"Events / {residuals_binning*1e3:1.0f} µm")
                residuals_axs[ieta][ichamber].set_title(eta_title)

                if not args.fwhm:
                    """ Fit residuals with two gaussians: """
                    x_centers = 0.5*(x_edges[1:]+x_edges[:-1])
                    popt = [ residuals_x_flat.mean(), counts.max(), residuals_x_flat.std() ]
                    #popt += [ residuals_x_flat.mean(), 0.1*counts.max(), 10*residuals_x_flat.std() ]
                    popt, pcov = curve_fit(gauss_pdf, x_centers, counts)
                    perr = np.sqrt(np.diag(pcov))
                    logging.info("  Residual fit parameters {}".format(popt))
                    logging.info("  Residual fit errors {}".format(perr))

                    fit_x = np.linspace(x_edges[0], x_edges[:-1], 1000)
                    residuals_axs[ieta][ichamber].plot(fit_x, gauss_pdf(fit_x, *popt), linewidth=2, color="red")

                    """ Add fit parameters """
                    pnames = ["$\mu_1$", "$C_1$", "$\sigma_1$", "$\mu_2$", "$C_2$", "$\sigma_2$"]
                    fit_text = ""
                    for i,(p,e,n) in enumerate(zip(popt, perr, pnames)):
                        fit_text += f"{n: >5} {p:1.2e} $\pm$ {e:1.2e}\n"
                    fit_text = fit_text[:-2]
                    residuals_axs[ieta][ichamber].text(
                        0.075, 0.9, fit_text,
                        transform = residuals_axs[ieta][ichamber].transAxes, ha="left", va="top",
                        fontsize=17, linespacing=2.1,
                        bbox=dict(boxstyle="square, pad=0.8", facecolor="white", linewidth=1.5)
                    )
                    residual_sigma = perr[2]
                else:
                    """ Calculate residuals FWHM """
                    residual_hm = fwhm(x_centers, counts)
                    residual_fwhm = residual_hm[1] - residual_hm[0]
                    residuals_axs[ieta][ichamber].text(
                        0.075, 0.9, f"{residual_fwhm:1.3f} FWHM mm",
                        transform = residuals_axs[ieta][ichamber].transAxes, ha="left", va="top",
                        fontsize=28, linespacing=2.1,
                    )
                    residual_sigma = residual_fwhm

                """ Plot correlations between propagated and residuals """
                prophits_rechits_axs[ieta][ichamber].hist2d(
                    ak.flatten(prophits_x_broadcast).to_numpy(), ak.flatten(rechits_x_broadcast).to_numpy(),
                    bins=130, #range=(-200,200)
                )
                prophits_rechits_axs[ieta][ichamber].set_xlabel("Prophit x (mm)")
                prophits_rechits_axs[ieta][ichamber].set_ylabel("Rechit x (mm)")
                prophits_rechits_axs[ieta][ichamber].set_title(eta_title)

                prophits_residuals_axs[ieta][ichamber].hist2d(
                    ak.flatten(prophits_x_broadcast).to_numpy(), ak.flatten(residuals_x).to_numpy(),
                    bins=130, range=((-50,50),(-5,5))
                )
                prophits_residuals_axs[ieta][ichamber].set_xlabel("Prophit x (mm)")
                prophits_residuals_axs[ieta][ichamber].set_ylabel("Residual x (mm)")
                prophits_residuals_axs[ieta][ichamber].set_title(eta_title)
                prophits_residuals_axs[ieta][ichamber].set_title(eta_title)

                """ Plot numerator and denominator profiles for efficiency calculation """
                matching_cut = 5*residual_sigma
                logging.info("    Applying matching cut {:1.2f} mm to residuals on chamber {}, eta {}".format(matching_cut, tested_chamber, tested_eta))
                prophit_region, prophit_bins = (-60, 60), 20
                prophit_binning = (prophit_region[1]-prophit_region[0])/prophit_bins*1e3
                event_has_match = ak.count_nonzero(abs(residuals_x)<matching_cut, axis=1)>0
                prophits_matching = prophits_x_broadcast[event_has_match]
                prophits_total = prophits_x_broadcast
                matching_counts, matching_edges, _ = matching_axs[ieta][ichamber].hist(
                    ak.flatten(prophits_matching),
                    bins=prophit_bins, range=prophit_region,
                    histtype="step", label="Matching", color="red"
                )
                total_counts, total_edges, _ = matching_axs[ieta][ichamber].hist(
                    ak.flatten(prophits_total),
                    bins=prophit_bins, range=prophit_region,
                    histtype="step", label="Total", color="blue"
                )
                matching_axs[ieta][ichamber].set_xlabel("Prophit x (mm)")
                matching_axs[ieta][ichamber].set_ylabel(f"Events / {prophit_binning:1.0f} µm")
                matching_axs[ieta][ichamber].legend()
                matching_axs[ieta][ichamber].set_title(eta_title)

                """ Plot efficiency profile """
                efficiency = matching_counts / total_counts
                x_centers = 0.5*(matching_edges[1:]+matching_edges[:-1])
                x_errors = np.diff(matching_edges)/np.sqrt(12)
                efficiency_errors = np.sqrt(efficiency*(1-efficiency)/total_counts)
                average_efficiency = np.mean(efficiency[~np.isnan(efficiency)])
                efficiency_profile_axs[ieta][ichamber].errorbar(x_centers, efficiency, xerr=x_errors, yerr=efficiency_errors, fmt="ok")
                efficiency_profile_axs[ieta][ichamber].text(
                    0.9, 0.1,
                    f"Average efficiency {average_efficiency:1.3f}",
                    transform=efficiency_profile_axs[ieta][ichamber].transAxes,
                    ha="right"
                )
                efficiency_profile_axs[ieta][ichamber].set_ylim(0., 1.05)
                efficiency_profile_axs[ieta][ichamber].set_xlabel("Propagated x (mm)")
                efficiency_profile_axs[ieta][ichamber].set_ylabel("Efficiency")
                efficiency_profile_axs[ieta][ichamber].set_title(eta_title)

                """ Plot efficiency trend vs residual cut """
                def get_efficiency(residual_cut):
                    prophit_window = abs(prophits_x_eta)<20
                    event_has_prophit = ak.count(prophits_x_eta[abs(prophits_x_eta)<20], axis=1)>0
                    event_has_match = ak.count_nonzero(abs(residuals_x[abs(prophits_x_broadcast)<20])<residual_cut, axis=1)>0
                    try:
                        count_match = ak.count_nonzero(event_has_match)
                        count_prophits = ak.count_nonzero(event_has_prophit)
                        logging.debug("        Cut {:01.2f} mm, matches {}, prophits {}".format(residual_cut, count_match, count_prophits))
                        #print("            cut         {}".format(residual_cut))
                        #print("            prophit     {}".format(prophits_x_eta[abs(prophits_x_eta)<20]))
                        #print("            has prophit {}".format(event_has_prophit), ak.count(event_has_prophit))
                        #print("            residuals   {}".format(residuals_x[abs(prophits_x_broadcast)<20]))
                        #print("            matches     {}".format(abs(residuals_x[abs(prophits_x_broadcast)<20])<residual_cut))
                        #print("            has match   {}".format(event_has_match), ak.count(event_has_match))
                        efficiency = count_match / count_prophits
                    except ZeroDivisionError:
                        logging.error("        Number of propagated hits was 0, setting efficiency to 0...")
                        return 0
                    return efficiency
                #residual_cut_range = np.arange(0.5, 10, 0.5)*residual_sigma
                residual_cut_range = np.arange(0.5, 10, 0.5) # cut up to 1 cm
                efficiency_scan = [ get_efficiency(residual_cut) for residual_cut in residual_cut_range ]
                efficiency_scan_axs[ieta][ichamber].plot(residual_cut_range, efficiency_scan, "o")
                efficiency_scan_axs[ieta][ichamber].set_xlabel("Residual cut (mm)")
                efficiency_scan_axs[ieta][ichamber].set_ylabel("Efficiency")
                efficiency_scan_axs[ieta][ichamber].set_title(eta_title)

                """ Plot a line at the saturation efficiency """
                """efficiency_plateau = max(efficiency_scan)
                efficiency_scan_axs[ieta][ichamber].plot(
                    residual_cut_range,
                    efficiency_plateau*np.ones_like(residual_cut_range),
                    "--", color="red"
                )
                efficiency_scan_axs[ieta][ichamber].text(
                    max(residual_cut_range), efficiency_plateau-0.025,
                    f"{efficiency_plateau*1e2:1.1f} %",
                    ha="right", va="top"
                )"""


        cluster_size_fig.tight_layout()
        logging.info("Saving cluster size to {}".format(args.odir / "cluster_size.png"))
        cluster_size_fig.savefig(args.odir / "cluster_size.png")
        #cluster_size_fig.savefig(args.odir / "cluster_size.pdf")

        profile_fig.tight_layout()
        logging.info("Saving profile to {}".format(args.odir / "profile.png"))
        profile_fig.savefig(args.odir / "profile.png")
        #profile_fig.savefig(args.odir / "profile.pdf")

        eta_fig.tight_layout()
        logging.info("Saving eta partitions to {}".format(args.odir / "eta.png"))
        eta_fig.savefig(args.odir / "eta.png")
        #eta_fig.savefig(args.odir / "eta.pdf")

        profile2d_fig.tight_layout()
        logging.info("Saving 2D profile to {}".format(args.odir / "profile2d.png"))
        profile2d_fig.savefig(args.odir / "profile2d.png")
        #profile2d_fig.savefig(args.odir / "profile2d.pdf")

        residuals_fig.tight_layout()
        logging.info("Saving residuals to {}".format(args.odir / "residuals.png"))
        residuals_fig.savefig(args.odir / "residuals.png")
        #residuals_fig.savefig(args.odir / "residuals.pdf")

        prophits_residuals_fig.tight_layout()
        logging.info("Saving prophits-residuals to {}".format(args.odir / "prophits_residuals.png"))
        prophits_residuals_fig.savefig(args.odir / "prophits_residuals.png")
        #prophits_residuals_fig.savefig(args.odir / "prophits_residuals.pdf")

        prophits_rechits_fig.tight_layout()
        logging.info("Saving prophits-rechits to {}".format(args.odir / "prophits_rechits.png"))
        prophits_rechits_fig.savefig(args.odir / "prophits_rechits.png")
        #prophits_rechits_fig.savefig(args.odir / "prophits_rechits.pdf")

        matching_fig.tight_layout()
        logging.info("Saving matching profile to {}".format(args.odir / "matching_profile.png"))
        matching_fig.savefig(args.odir / "matching_profile.png")
        #matching_fig.savefig(args.odir / "matching_profile.pdf")

        efficiency_profile_fig.tight_layout()
        logging.info("Saving efficiency profile to {}".format(args.odir / "efficiency_profile.png"))
        efficiency_profile_fig.savefig(args.odir / "efficiency_profile.png")
        #efficiency_profile_fig.savefig(args.odir / "efficiency_profile.pdf")

        efficiency_scan_fig.tight_layout()
        logging.info("Saving efficiency scan to {}".format(args.odir / "efficiency_scan.png"))
        efficiency_scan_fig.savefig(args.odir / "efficiency_scan.png")
        #efficiency_scan_fig.savefig(args.odir / "efficiency_scan.pdf")

if __name__=='__main__': main()
