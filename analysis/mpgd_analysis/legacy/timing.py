import os, sys, pathlib
import argparse
import logging
from tqdm import tqdm

import uproot
import numpy as np
import pandas as pd
import awkward as ak

from scipy.optimize import curve_fit
from scipy.stats import binned_statistic
from scipy.stats import norm

import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import mplhep as hep

plt.style.use(hep.style.ROOT)
plt.rcParams.update({"font.size": 32})
plt.rcParams.update({"image.cmap": "Purples"})
logging.getLogger('matplotlib.font_manager').disabled = True
hep.cms.label()#, data=<True|False>, lumi=50, year=2017)

gauss_pdf = lambda x, k , mu, s: k * norm.pdf(x, mu, s)
gauss2_pdf = lambda x, k1, m1, s1, k2, m2, s2: gauss_pdf(x, m1, k1, s1) + gauss_pdf(x, m2, k2, s2)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("ifile", type=pathlib.Path)
    parser.add_argument("odir", type=pathlib.Path)
    parser.add_argument("--fit", nargs="+", type=int, help="Range to fit gaussian peak")
    parser.add_argument("--range", nargs="+", type=int, help="Range to determine optimal latency")
    parser.add_argument("-m", "--multiplicities", action="store_true")
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("-n", "--events", type=int, default=-1)
    args = parser.parse_args()

    os.makedirs(args.odir, exist_ok=True)
    if args.verbose: logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    logging.basicConfig(level=logging.INFO, format="%(message)s")

    with uproot.open(args.ifile) as digi_file:
        digi_tree = digi_file["outputtree"]
        if args.verbose: digi_tree.show()

        logging.info("Reading tree...")
        events_df = digi_tree.arrays(
            [
            "runParameter",
            "slot", "OH", "VFAT", "CH",
            "digiChamber", "digiEta", "digiStrip",
            ], entry_stop=args.events, library="pd"
        )
        events_df = events_df[events_df.runParameter>0]

        latency = digi_tree["runParameter"].array()
        latency = latency[latency>0]
        latency_range = [ak.min(latency), ak.max(latency)]
        logging.info("Latency range {} - {}".format(latency_range[0], latency_range[1]))

        latency_triggers, latency_edges = np.histogram(
            latency.to_numpy(),
            range=(latency_range[0]-0.5, latency_range[1]+0.5),
            bins=latency_range[1]-latency_range[0]+1
        )
        latency_centers = 0.5*(latency_edges[1:]+latency_edges[:-1])
        weights_df = pd.DataFrame(
            {"weight": 1/latency_triggers },
            index=latency_centers.astype(int)
        )
        events_df = pd.merge(
            events_df, weights_df,
            left_on="runParameter", right_index=True, how="left", sort=False
        )
        logging.debug(events_df)

        """if args.multiplicities:
            
            for oh in np.unique(ak.flatten(ohs)):
                        
                vfats_oh = vfats[ohs==oh]
                channels_oh = channels[ohs==oh]
                
                latency_multiplicity_fig = plt.figure(figsize=(12*4,10*6))
                for vfat in np.unique(ak.flatten(vfats_oh)):

                    channels_vfat = channels_oh[vfats_oh==vfat]
                    print(f"channels vfat {vfat}", channels_vfat)
                    multiplicities = ak.count(channels_vfat, axis=1)
                    count = ak.count_nonzero(multiplicities)
                    count = ak.sum(multiplicities)

                    print("latency", latency, len(latency))
                    print("latency", multiplicities, len(multiplicities))

                    latency_multiplicities_ax = latency_multiplicity_fig.add_subplot(6, 4, vfat+1)
                    latency_multiplicities_ax.hist(multiplicities[latency<=52], label="latency < 52", alpha=0.3, bins=128, range=(0,128))
                    latency_multiplicities_ax.hist(multiplicities[latency>52], label="latency > 52", alpha=0.3, bins=128, range=(0,128))
                    latency_multiplicities_ax.legend()
                    latency_multiplicities_ax.set_xlabel("Event multiplicity")
                    latency_multiplicities_ax.set_yscale("log")
                    #latency_multiplicities_ax.hist2d(np.array(latency), np.array(multiplicities), range=((45,60),(0,100)), norm=colors.LogNorm())
                    #latency_multiplicities_ax.set_xlabel("Latency (BX)")
                    #latency_multiplicities_ax.set_ylabel("Event multiplicity")
                    #latency_multiplicities_ax.set_zlim(1, 10e3)
                
                latency_multiplicity_fig.tight_layout()
                latency_multiplicity_fig.savefig(args.odir / "latency_multiplicity_fed{}_oh{}.png".format(args.fed, oh))
        """

        if args.range: latency_range = args.range
        def analyze_oh(oh_df):

            def analyze_vfat(vfat_df):

                vfat_df = vfat_df.reset_index()
                vfat = vfat_df["VFAT"].unique()[0]

                try:
                    ax = oh_axs.flat[oh_vfats_indices[vfat]]
                except IndexError:
                    # The VFAT does not exist for this OH, we can skip it...
                    return
                """ Plot total multiplicities per latency and determine optimal latency """
                logging.debug("All hits:")
                logging.debug(vfat_df)
                latency_counts, latency_bins, _ = ax.hist(
                    vfat_df["runParameter"],
                    weights=vfat_df["weight"],
                    bins=latency_range[1]-latency_range[0]+1,
                    range=(latency_range[0]-0.5, latency_range[1]+0.5),
                    histtype="step", linewidth=2, color="blue", label="Hits"
                )
                latency_centers = 0.5*(latency_bins[1:]+latency_bins[:-1])

                """ Plot event counts per latency """
                vfat_df_unique = vfat_df.drop_duplicates("entry")
                logging.debug("One hit per event:")
                logging.debug(vfat_df_unique)
                ax.hist(
                    vfat_df_unique["runParameter"],
                    weights=vfat_df_unique["weight"],
                    bins=latency_range[1]-latency_range[0]+1,
                    range=(latency_range[0]-0.5, latency_range[1]+0.5),
                    histtype="step", linewidth=2, color="green", label="Events"
                )
                ax.set_xlim(0, latency_range[1])
                ax.legend(loc="upper left")
                ax.set_title("VFAT {}".format(vfat))
                ax.set_xlabel("Latency (BX)")
                ax.set_ylabel("Fraction of events")

                """ Fit with gaussian """
                if args.fit:
                    fit_range = np.array(args.fit).astype(float)
                    latency_filter = (latency_centers>latency_range[0])&(latency_centers<latency_range[1])
                    popt = [ np.max(latency_counts), 0.5*np.sum(fit_range), np.diff(fit_range)[0] ]
                    try:
                        popt, pcov = curve_fit(gauss_pdf, latency_centers[latency_filter], latency_counts[latency_filter], p0=popt)
                        perr = np.sqrt(np.diag(pcov))
                        x = np.linspace(*fit_range, 100)
                        ax.plot(x, gauss_pdf(x, *popt), "-", color="red")
                        logging.debug(popt)
                        logging.debug(perr)
                        ax.annotate(
                            "Mean {:1.1f} $\pm$ {:1.1f}\nSigma {:1.1f} $\pm$ {:1.1f}".format(popt[1], perr[1], popt[2], perr[2]),
                            (0.05, 0.7), xycoords="axes fraction", va="top"
                        )
                    except TypeError:
                        logging.info("Fit failed, skipping")
                    optimal_latency = popt[1]
                elif args.range:
                    latency_filter = (latency_centers>args.range[0])&(latency_centers<args.range[1])
                    ls, cs = latency_centers[latency_filter], latency_counts[latency_filter]
                    optimal_latency = (np.sum(ls*cs)/np.sum(cs)).astype(int)
                    optimal_latency = latency_centers[np.argmax(latency_counts)].astype(int)
                    logging.info("Analyzed slot {}, OH {}, VFAT {}: latency {}".format(slot, oh, vfat, optimal_latency))

                    ax.annotate(
                        "Optimal latency {}".format(optimal_latency),
                        (0.05,0.7), xycoords="axes fraction"
                    )
                else:
                    raise Error("Could not determine range for latency peak. You need to use either the --fit or the --range options.")
                        
                return pd.Series(
                        (1479, slot, oh, vfat, optimal_latency),
                        index=["fed", "slot", "oh", "vfat", "latency"]
                        )

                latencies, counts = df["latency"], df["counts"]
                #optimal_latency = int(latencies[np.argmax(counts)])
                average_latency = (np.sum(latencies*counts)/np.sum(counts)).astype(int)
                
                latency_ax = latency_figs[oh].add_subplot(6, 4, vfat+1)
                print("oh {}, vfat {}, optimal latency {}".format(oh, vfat, average_latency))
                latency_ax.bar(latencies, counts, width=1, alpha=0.4, label=f"{average_latency} BX".format(average_latency))
                latency_ax.set_xlim(ak.min(latency), ak.max(latency))
                latency_ax.set_xlabel("Latency (BX)")
                latency_ax.set_ylabel("Counts")
                latency_ax.set_title("OH {}, VFAT {}".format(oh, vfat))
                latency_ax.legend()

                """ Fit and plot gaussian """
                """gauss = lambda x, a, mu, sigma: a * norm.pdf(x, mu, sigma)
                p0 = [100, latencies.mean(), latencies.std()]
                try:
                    popt, pcov = curve_fit(gauss, latencies, counts, p0=p0)
                    perr = np.sqrt(np.diag(pcov))
                    latency_ax.text(
                        0.1, 0.9,
                        f"Mean {popt[1]:1.2f} $\pm$ {perr[1]:1.2f} BX\n"+\
                        f"Sigma {popt[2]:1.2f} $\pm$ {perr[2]:1.2f} BX",
                        transform=latency_ax.transAxes,
                        ha="left", va="top"
                        )
                    xs = np.linspace(0, 100, 1000)
                    latency_ax.plot(xs, gauss(xs, *popt), "-", color="red")
                except (TypeError, RuntimeError) as e:
                    print("Error while fitting: '{}'. skipping OH {}, VFAT {}...".format(e, oh, vfat))"""

            slot, oh = oh_df["slot"].unique()[0], oh_df["OH"].unique()[0]
            logging.debug(oh_df)

            oh_vfats = oh_df["VFAT"].unique()
            oh_vfats_indices = { vfat:i for i,vfat in enumerate(sorted(oh_vfats)) }
            nrows = 3
            ncols = int(len(oh_vfats)/nrows)
            oh_fig, oh_axs = plt.subplots(ncols=ncols, nrows=nrows, figsize=(11*ncols, 9*nrows))

            latency_df_oh = oh_df.groupby("VFAT", as_index=False, group_keys=False).apply(analyze_vfat)

            oh_fig.tight_layout()
            oh_fig.savefig(args.odir / "latency_slot{}_oh{}.png".format(slot, oh))

            return latency_df_oh.drop("VFAT", axis=1)

        latency_df = events_df.groupby(["slot", "OH"], as_index=False, group_keys=False).apply(analyze_oh)
        print(latency_df)
        latency_df.to_csv(args.odir / "latencies.cfg", sep=";", index=False)
        logging.info("Optimal latencies saved to {}".format(args.odir / "latencies.cfg"))

if __name__=='__main__': main()
