import logging

import numpy as np
import zfit

def lin_interp(x, y, i, half):
    return x[i] + (x[i+1] - x[i]) * ((half - y[i]) / (y[i+1] - y[i]))

def fwhm(x, y):
    half = max(y)/2.0
    signs = np.sign(y-half)
    zero_crossings = (signs[0:-2] != signs[1:-1])
    zero_crossings_i = np.where(zero_crossings)[0]
    try:
        return [lin_interp(x, y, zero_crossings_i[0], half), lin_interp(x, y, zero_crossings_i[1], half)]
    except IndexError:
        return [-0.3, 0.3]

""" Fit residuals with two gaussians """
def fit_residuals_2gauss(residuals, residual_bins, residual_range):
    obs = zfit.Space("x", limits=residual_range)
    zfit.Parameter._existing_params.clear()
    try:
        """ Define fit parameters: """
        mu_signal = zfit.Parameter("mu_signal" , 0, -10, 10)
        sigma_signal = zfit.Parameter("sigma_signal", 3,  0, 10)
        mu_bkg = zfit.Parameter("mu_bkg" , 0, -10, 10)
        sigma_bkg = zfit.Parameter("sigma_bkg" , 6, 1, 30)
        """ Define gaussian models: """
        signal_gauss = zfit.pdf.Gauss(obs=obs, mu=mu_signal, sigma=sigma_signal)
        background_gauss = zfit.pdf.Gauss(obs=obs, mu=mu_bkg, sigma=sigma_bkg)
        """ Define sum model: """
        sig_frac = zfit.Parameter("sig_frac", 0.8, 0, 1)
        model = zfit.pdf.SumPDF([signal_gauss, background_gauss], [sig_frac])
    except zfit.util.exception.NameAlreadyTakenError:
        logging.warning("Fit model already defined, using pre-defined one...")

    """ Fit to residuals: """
    data = zfit.Data.from_numpy(obs=obs, array=residuals)
    nll = zfit.loss.UnbinnedNLL(model=model, data=data)
    minimizer = zfit.minimize.Minuit()
    result = minimizer.minimize(nll)

    """ Calculate scale of the model to plot the fit curve later: """
    scale = len(residuals) / residual_bins * data.data_range.area()

    return model, result, scale
