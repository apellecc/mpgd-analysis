import argparse
import pathlib
 
from mpgd_analysis.qc8 import occupancy as qc8_occupancy
from mpgd_analysis.qc8 import tracks as qc8_tracks
from mpgd_analysis.qc8 import alignment as qc8_alignment
from mpgd_analysis.qc8 import hv_scan as qc8_hv_scan
from mpgd_analysis.me0 import tracks as me0_tracks
from mpgd_analysis.hcal import rechits as hcal_rechits

def main():
    parser = argparse.ArgumentParser(
            prog="mpgd-analysis",
            description="Analyze MPGD data"
            )
    subparsers = parser.add_subparsers(required=True)

    """ CMS GEM QC8 analysis """
    qc8_parser = subparsers.add_parser("qc8", help="Run analysis for CMS GEM QC8")
    qc8_subparsers = qc8_parser.add_subparsers(required=True)

    """ Plot QC8 digi occupancy """
    qc8_occupancy_parser = qc8_subparsers.add_parser("occupancy", help="Plot QC8 occupancy")
    qc8_occupancy_parser.add_argument("ifile", type=pathlib.Path, help="Digi file")
    qc8_occupancy_parser.add_argument("odir", type=pathlib.Path, help="Output directory")
    qc8_occupancy_parser.add_argument("-n", "--events", type=int, help="Number of events to analyze")
    qc8_occupancy_parser.add_argument("-v", "--verbose", action="store_true", help="Verbose mode")
    qc8_occupancy_parser.set_defaults(
            func = lambda args: qc8_occupancy.analyze(args.ifile, args.odir, args.events, args.verbose)
            )

    """ Analyze QC8 tracks """
    qc8_tracks_parser = qc8_subparsers.add_parser("tracks", help="Analyze QC8 tracks")
    qc8_tracks_parser.add_argument("ifile", type=pathlib.Path, help="Track file")
    qc8_tracks_parser.add_argument("odir", type=pathlib.Path, help="Output directory")
    qc8_tracks_parser.add_argument("--efficiency", action="store_true", help="Calculate efficiency")
    qc8_tracks_parser.add_argument("-n", "--events", type=int, help="Number of events to analyze")
    qc8_tracks_parser.add_argument("--fit", action="store_true", help="Fit residuals")
    qc8_tracks_parser.add_argument("-v", "--verbose", action="store_true", help="Verbose mode")
    qc8_tracks_parser.set_defaults(
            func = lambda args: qc8_tracks.analyze(args.ifile, args.odir, args.events, args.efficiency, args.fit, args.verbose)
            )

    """ Apply QC8 alignment to geometry """
    qc8_alignment_parser  = qc8_subparsers.add_parser("apply-alignment", help="Apply QC8 alignment")
    qc8_alignment_parser .add_argument("ifile", type=pathlib.Path, help="CSV file containing alignment constants")
    qc8_alignment_parser .add_argument("geometry_file", metavar="geometry-file", type=pathlib.Path, help="Geometry xml file where to apply the alignment")
    qc8_alignment_parser .add_argument("ofile", type=pathlib.Path, help="Output geometry file with alignment applied")
    qc8_alignment_parser .add_argument("--chamber", type=int, default=-1, help="Apply correction only for this chamber")
    qc8_alignment_parser .add_argument("-v", "--verbose", action="store_true", help="Verbose mode")
    qc8_alignment_parser .set_defaults(
            func = lambda args: qc8_alignment.apply_alignment(args.ifile, args.geometry_file, args.ofile, args.chamber, args.verbose)
            )

    """ Plot QC8 efficiency scan """
    qc8_scan_parser = qc8_subparsers.add_parser("hv-scan", help="Plot QC8 HV scan results")
    qc8_scan_parser.add_argument("ifile", type=pathlib.Path, help="Scan input csv file")
    qc8_scan_parser.add_argument("odir", type=pathlib.Path, help="Output directory")
    qc8_scan_parser.add_argument("--efficiency", required=True, type=pathlib.Path, nargs="+", help="Efficiency csv file for each run")
    qc8_scan_parser.add_argument("-v", "--verbose", action="store_true", help="Verbose mode")
    qc8_scan_parser.set_defaults(
            func = lambda args: qc8_hv_scan.analyze(args.ifile, args.odir, args.efficiency, args.verbose)
            )


    """ CMS GEM ME0 analysis """
    me0_parser = subparsers.add_parser("me0", help="Run analysis for CMS GEM ME0 stack")
    me0_subparsers = me0_parser.add_subparsers(required=True)

    """ Analyze ME0 stack tracks """
    me0_tracks_parser = me0_subparsers.add_parser("tracks", help="Analyze ME0 stack tracks")
    me0_tracks_parser.add_argument("ifile", type=pathlib.Path, help="Track file")
    me0_tracks_parser.add_argument("odir", type=pathlib.Path, help="Output directory")
    me0_tracks_parser.add_argument("--efficiency", action="store_true", help="Calculate efficiency")
    me0_tracks_parser.add_argument("-n", "--events", type=int, help="Number of events to analyze")
    me0_tracks_parser.add_argument("--fit", action="store_true", help="Fit residuals")
    me0_tracks_parser.add_argument("-v", "--verbose", action="store_true", help="Verbose mode")
    me0_tracks_parser.set_defaults(
            func = lambda args: me0_tracks.analyze(args.ifile, args.odir, args.events, args.efficiency, args.fit, args.verbose)
            )


    """ MPGD-HCAL analysis """
    hcal_parser = subparsers.add_parser("hcal", help="Run analysis for MPGD-HCAL")
    hcal_subparsers = hcal_parser.add_subparsers(required=True)

    """ Analyze HCAL rechits """
    hcal_rechits_parser = hcal_subparsers.add_parser("rechits", help="Analyze HCAL rechits")
    hcal_rechits_parser.add_argument("ifile", type=pathlib.Path, help="Rechit file")
    hcal_rechits_parser.add_argument("odir", type=pathlib.Path, help="Output directory")
    hcal_rechits_parser.add_argument("--chambers-x", type=int, nargs="+", required=True, help="Index of x chambers to analyze")
    hcal_rechits_parser.add_argument("--chambers-y", type=int, nargs="+", required=True, help="Index of y chambers to analyze")
    hcal_rechits_parser.add_argument("-n", "--events", type=int, help="Number of events to analyze")
    hcal_rechits_parser.add_argument("-v", "--verbose", action="store_true", help="Verbose mode")
    hcal_rechits_parser.set_defaults(
            func = lambda args: hcal_rechits.analyze(args.ifile, args.odir, [args.chambers_x, args.chambers_y], args.events, args.verbose)
            )

    args = parser.parse_args()
    args.func(args)

if __name__=="__main__":
    main()
