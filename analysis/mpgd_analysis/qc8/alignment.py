import logging
import xml.etree.ElementTree

import numpy as np
import pandas as pd

from mpgd_analysis.qc8 import tools as qc8_tools

def apply_alignment(ifile, geometry_file, ofile, chosen_chamber, verbose):
    """ Applies alignment constants in csv file to xml geometry file """

    if verbose: logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    else: logging.basicConfig(level=logging.INFO, format="%(message)s")

    residuals_df = pd.read_csv(ifile, sep=";")
    if chosen_chamber > 0:
        residuals_df = residuals_df[residuals_df.chamber==chosen_chamber]
    residuals_df["module"], residuals_df["eta"] = qc8_tools.to_modules(np.array(residuals_df.chamber), np.array(residuals_df.eta))
    logging.debug(residuals_df)

    corrections_df = residuals_df.groupby("module").apply(lambda df: df["mean"].mean()).to_frame().reset_index()
    corrections_df.rename(columns={0: "x"}, inplace=True)
    logging.debug(corrections_df)

    geometry_file = xml.etree.ElementTree.parse(geometry_file)
    geometry_root = geometry_file.getroot()
    for detector_node in geometry_root:
        if detector_node.tag != "detector":
            logging.debug("Skipping node {} because it is not a detector".format(detector_node.tag))
            continue
        detector_id = int(detector_node.attrib["id"])
        x_correction = corrections_df.x[corrections_df.module==detector_id]
        if x_correction.size == 0: x_correction = 0
        elif x_correction.size == 1: x_correction = x_correction.item()
        else: raise ValueError("More than one correction found for module {}".format(detector_id))
        logging.debug("Setting x correction {:.2e} to detector with ID {} and label {}...".format(x_correction, detector_id, detector_node.attrib["label"]))

        for alignment_node in detector_node:
            if alignment_node.tag != "alignment":
                logging.debug("  Skipping node {} because it is not alignment".format(alignment_node.tag))
                continue
            for coordinate_node in alignment_node:
                if coordinate_node.tag != "x":
                    logging.debug("    Skipping node {} because it is not x coordinate".format(coordinate_node.tag))
                    continue
                previous_value = float(coordinate_node.text)
                new_value = previous_value - x_correction
                logging.debug("    Previous x value was {:.2e}, new value is {:.2e}".format(previous_value, new_value))
                coordinate_node.text = str(new_value)
    geometry_file.write(ofile)
    logging.info("Output geometry file created in {}".format(ofile))
