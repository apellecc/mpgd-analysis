import awkward as ak
import numpy as np
import pandas as pd
import zfit

import logging

MODULE_NAMES = [ "M1", "M2", "M3", "M4" ]

def general_astype(val, t):
    if type(val) is ak.Array or type(val) is np.ndarray:
        return ak.values_astype(val, t)
    elif type(val) is pd.Series:
        return val.astype(t)
    else:
        return t(val)

""" Convert module and eta partition name to GE2/1 conventions """
def to_layers(chamber, eta):
    layer = general_astype(chamber / 4, int)
    module = general_astype(chamber % 4, int)
    eta = general_astype(eta + 4 * (3-module), int)
    return layer, module, eta

""" Inverse of to_layers: find detector ID and local eta from layer and global eta """
def to_modules(layer, eta):
    chamber = 4*layer + general_astype((16-eta)/4, int)
    eta = (eta-1) % 4 + 1
    return chamber, eta

""" Fit residuals with two gaussians """
def fit_residuals_2gauss(residuals, residual_bins, residual_range):
    obs = zfit.Space("x", limits=residual_range)

    zfit.Parameter._existing_params.clear()
    try:
        """ Define fit parameters: """
        mu_signal = zfit.Parameter("mu_signal" , 0, -10, 10)
        sigma_signal = zfit.Parameter("sigma_signal", 3,  0, 10)
        mu_bkg = zfit.Parameter("mu_bkg" , 0, -10, 10)
        sigma_bkg = zfit.Parameter("sigma_bkg" , 6, 1, 30)
        """ Define gaussian models: """
        signal_gauss = zfit.pdf.Gauss(obs=obs, mu=mu_signal, sigma=sigma_signal)
        background_gauss = zfit.pdf.Gauss(obs=obs, mu=mu_bkg, sigma=sigma_bkg)
        """ Define sum model: """
        sig_frac = zfit.Parameter("sig_frac", 0.8, 0, 1)
        model = zfit.pdf.SumPDF([signal_gauss, background_gauss], [sig_frac])
    except zfit.util.exception.NameAlreadyTakenError:
        logging.warning("Fit model already defined, using pre-defined one...")

    """ Fit to residuals: """
    data = zfit.Data.from_numpy(obs=obs, array=residuals)
    nll = zfit.loss.UnbinnedNLL(model=model, data=data)
    minimizer = zfit.minimize.Minuit()
    result = minimizer.minimize(nll)

    """ Calculate scale of the model to plot the fit curve later: """
    scale = len(residuals) / residual_bins * data.data_range.area()

    return model, result, scale
