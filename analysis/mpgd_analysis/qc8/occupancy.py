import logging
import os

import awkward as ak
import numpy as np
import pandas as pd
import uproot

import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import mplhep as hep

plt.style.use(hep.style.ROOT)
#plt.rcParams.update({"font.size": 32})
#plt.rcParams.update({"image.cmap": "Purples"})
logging.getLogger('matplotlib.font_manager').disabled = True

from mpgd_analysis.qc8 import tools as qc8_tools
from mpgd_analysis.common import tools as common_tools
from mpgd_analysis.common import geometry 

def analyze(ifile, odir, nevents, verbose):

    os.makedirs(odir, exist_ok=True)

    if verbose: logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    else: logging.basicConfig(level=logging.INFO, format="%(message)s")

    """ Load xml geometry files """
    setup = geometry.SetupGeometry("qc8-front")

    with uproot.open(ifile) as digi_file:

        logging.info("Opening file {}...".format(ifile))
        digi_tree = digi_file["outputtree"]
        logging.info("Reading tree...")
        digi_df = ak.to_dataframe(digi_tree.arrays(
            ["digiStripChamber", "digiStripEta", "digiStrip", "rawVFAT"],
            library="ak",  entry_stop=nevents
        )).reset_index()

    """ Apply GE2/1 name conventions to chambers """
    digi_df["digiChamber"], digi_df["digiModule"], digi_df["digiEta"] = qc8_tools.to_layers(digi_df.digiStripChamber, digi_df.digiStripEta)

    list_chambers = digi_df.digiChamber.sort_values().unique()
    list_modules = digi_df.digiModule.sort_values().unique()
    list_eta = digi_df.digiEta.sort_values().unique()
    list_strips = digi_df.digiStrip.sort_values().unique()
    n_chambers = len(list_chambers)
    n_modules = len(list_modules)
    n_eta = len(list_eta)
    n_strips = len(list_strips)
    logging.info("{:2d} chambers in setup:       {}".format(n_chambers, list_chambers))
    logging.info("{:2d} modules in setup:        {}".format(n_modules, list_modules))
    logging.info("{:2d} eta partitions in setup: {}".format(n_eta, list_eta))

    strip_range, strip_bins = (0.5, n_strips+0.5), n_strips
    eta_range, eta_bins = (0.5, n_eta+0.5), n_eta

    """ Plot digi occupancy for a single chamber """
    def plot_chamber(chamber_df):
        chamber = chamber_df.digiChamber.iloc[0]
        logging.debug("Plotting occupancy for chamber {}...".format(chamber))
        chamber_fig, chamber_axs = plt.subplots(nrows=4, ncols=4, figsize=(11*4, 9*4))
        chamber_2d_fig, chamber_2d_ax = plt.subplots(nrows=1, ncols=1, figsize=(11, 11))

        """ Plot digi occupancy by eta partition """
        def plot_eta(eta_df):
            eta = eta_df.digiEta.iloc[0]
            module = eta_df.digiModule.iloc[0]
            module_name = qc8_tools.MODULE_NAMES[module]
            logging.debug("  Plotting occupancy for module {} eta {}...".format(module_name, eta))
            icol, irow = (eta-1) % 4, int((16-eta) / 4)

            """ Plot digi occupancy by VFAT """
            def plot_vfat(vfat_df):
                vfat = vfat_df.rawVFAT.iloc[0]
                chamber_axs[icol][irow].hist(
                    vfat_df.digiStrip, range=strip_range, bins=strip_bins,
                    histtype="step", label="VFAT {}".format(vfat)
                )

            eta_df.groupby("rawVFAT").apply(plot_vfat)
            chamber_axs[icol][irow].legend(loc="upper right")
            chamber_axs[icol][irow].set_xlabel("Strip")
            chamber_axs[icol][irow].set_ylabel("Occupancy")
            chamber_axs[icol][irow].set_title("{} eta {}".format(module_name, eta))

        """ Plot 2D digi occupancy """
        _, _, _, occupancy_im = chamber_2d_ax.hist2d(
            chamber_df.digiStrip, chamber_df.digiEta,
            range=(strip_range, eta_range), bins=(strip_bins, eta_bins)
        )
        divider = make_axes_locatable(chamber_2d_ax)
        occupancy_cax = divider.append_axes("right", size="5%", pad=0.1)
        chamber_2d_fig.colorbar(occupancy_im, cax=occupancy_cax, orientation="vertical")

        chamber_2d_ax.set_xlabel("Strip")
        chamber_2d_ax.set_ylabel("Eta")
        chamber_2d_fig.suptitle("Detector {}".format(chamber))
        chamber_2d_ofile = odir / "occupancy2d-chamber{}.png".format(chamber)
        logging.info("Saving 2D occupancy for chamber {} to {}...".format(chamber, chamber_2d_ofile))
        chamber_2d_fig.savefig(chamber_2d_ofile)

        chamber_df.groupby("digiEta").apply(plot_eta)
        chamber_fig.suptitle("Detector {}".format(chamber))
        chamber_fig.tight_layout()
        chamber_ofile = odir / "occupancy-chamber{}.png".format(chamber)
        logging.info("Saving occupancy for chamber {} to {}...".format(chamber, chamber_ofile))
        chamber_fig.savefig(chamber_ofile)

    digi_df.groupby("digiChamber").apply(plot_chamber)

