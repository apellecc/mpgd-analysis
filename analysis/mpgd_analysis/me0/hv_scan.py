import logging
import os
import sys

import awkward as ak
import numpy as np
import pandas as pd
import uproot

import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import mplhep as hep

plt.style.use(hep.style.ROOT)
#plt.rcParams.update({"font.size": 32})
#plt.rcParams.update({"image.cmap": "Purples"})
logging.getLogger('matplotlib.font_manager').disabled = True

from mpgd_analysis.qc8 import tools as qc8_tools
from mpgd_analysis.common import tools as common_tools
from mpgd_analysis.common import geometry 

def read_efficiency(efficiency_csv):
    efficiency_df = pd.read_csv(efficiency_csv, sep=";")
    try: 
        run_number = int(efficiency_csv.stem)
    except ValueError as e:
        logging.error("File {} should have the name of a run number".format(efficiency_csv))
        sys.exit(1)
    efficiency_df["run"] = run_number
    return efficiency_df

def analyze(ifile, odir, efficiency_files, verbose):

    os.makedirs(odir/"residuals", exist_ok=True)

    if verbose: logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    else: logging.basicConfig(level=logging.INFO, format="%(message)s")

    scan_df = pd.read_csv(ifile, sep=";")

    efficiency_df = pd.concat([
        read_efficiency(f) for f in efficiency_files
        ])

    """ Merge the two databases to obtain HV for each layer """
    efficiency_df = efficiency_df.merge(scan_df, on=["run", "layer"])
    efficiency_df = efficiency_df[["layer", "hv", "eta", "phi", "matching", "prophits"]]

    """ Calculate efficiency VFAT by VFAT """
    efficiency_df["efficiency"] = efficiency_df.matching / efficiency_df.prophits
    efficiency_df["err_efficiency"] = np.sqrt(efficiency_df.efficiency*(1-efficiency_df.efficiency)/efficiency_df.prophits)

    """ Plot efficiency VFAT by VFAT for each layer """
    def plot_layer(layer_df):

        layer = layer_df["layer"].iloc[0]
        phis = layer_df.phi.unique()
        etas = layer_df.eta.unique()
        nrows, ncols = 4, 4

        eta_indices = {eta:i for i,eta in enumerate(etas)}
        phi_indices = {phi:i for i,phi in enumerate(phis)}

        efficiency_fig, efficiency_axs = plt.subplots(ncols=ncols, nrows=nrows, figsize=(11*ncols, 9*nrows))
        """ Plot efficiency scan for a single VFAT """
        def plot_vfat(vfat_df):
            eta = vfat_df["eta"].iloc[0]
            phi = vfat_df["phi"].iloc[0]
            vfat_df.sort_values(by="hv", inplace=True)
            efficiency_axs.flat[eta_indices[eta]].errorbar(
                    vfat_df.hv, vfat_df.efficiency, yerr=vfat_df.err_efficiency,
                    fmt="o-", label="$\phi$ = {}".format(phi)
                    )
            efficiency_axs.flat[eta_indices[eta]].set_xlabel("Equivalent divider current (µA)")
            efficiency_axs.flat[eta_indices[eta]].set_ylabel("Efficiency")
            efficiency_axs.flat[eta_indices[eta]].set_title("Eta {}".format(eta, phi))
            efficiency_axs.flat[eta_indices[eta]].legend()

        layer_df.groupby(["eta", "phi"]).apply(plot_vfat)
        efficiency_fig.suptitle("Layer {}".format(layer))
        efficiency_fig.tight_layout()
        efficiency_ofile = odir / "efficiency_layer{}.png".format(layer)
        logging.info("Saving efficiency scan for layer {} to {}".format(layer, efficiency_ofile))
        efficiency_fig.savefig(efficiency_ofile)

    efficiency_df.groupby("layer").apply(plot_layer)
