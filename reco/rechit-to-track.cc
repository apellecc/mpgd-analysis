#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <array>
#include <bitset>
#include <signal.h>
#include <math.h>
#include <sys/stat.h>

#include <chrono>
#include <thread>

#include <TFile.h>
#include <TTree.h>

#include "CLI11.hpp"
#include "progressbar.h"
#include "logging.h"

#include "Rechit.h"
#include "RechitEvent.h"
#include "TrackEvent.h"
#include "Hit.h"
#include "Track.h"
#include "DetectorGeometry.h"
#include "SetupGeometry.h"

bool isInterrupted = false;
void interruptHandler(int dummy) {
    isInterrupted = true;
}
bool logging::verbose = false;

class RechitFileReader {

    public:

        RechitFileReader(std::string ifile) {

            rechitFile = new TFile(ifile.c_str(), "READ");     
            rechitTree = (TTree *) rechitFile->Get("rechitTree");

            // Rechit variable branches
            rechitTree->SetBranchAddress("orbitNumber", &(m_rechitEvent.orbitNumber));
            rechitTree->SetBranchAddress("bunchCounter", &(m_rechitEvent.bunchCounter));
            rechitTree->SetBranchAddress("eventCounter", &(m_rechitEvent.eventCounter));
            rechitTree->SetBranchAddress("clusterStripChamber", &(m_rechitEvent.clusterStripChamber));
            rechitTree->SetBranchAddress("clusterStripEta", &(m_rechitEvent.clusterStripEta));
            rechitTree->SetBranchAddress("clusterStripCenter", &(m_rechitEvent.clusterStripCenter));
            rechitTree->SetBranchAddress("clusterStripSize", &(m_rechitEvent.clusterStripSize));
            rechitTree->SetBranchAddress("clusterPadChamber", &(m_rechitEvent.clusterPadChamber));
            rechitTree->SetBranchAddress("clusterPadCenterX", &(m_rechitEvent.clusterPadCenterX));
            rechitTree->SetBranchAddress("clusterPadCenterY", &(m_rechitEvent.clusterPadCenterY));
            rechitTree->SetBranchAddress("clusterPadSizeX", &(m_rechitEvent.clusterPadSizeX));
            rechitTree->SetBranchAddress("clusterPadSizeY", &(m_rechitEvent.clusterPadSizeY));
            rechitTree->SetBranchAddress("rechitChamber", &(m_rechitEvent.rechitChamber));
            rechitTree->SetBranchAddress("rechitEta", &(m_rechitEvent.rechitEta));
            rechitTree->SetBranchAddress("rechitX", &(m_rechitEvent.rechitX));
            rechitTree->SetBranchAddress("rechitY", &(m_rechitEvent.rechitY));
            rechitTree->SetBranchAddress("rechitErrorX", &(m_rechitEvent.rechitErrorX));
            rechitTree->SetBranchAddress("rechitErrorY", &(m_rechitEvent.rechitErrorY));
            rechitTree->SetBranchAddress("rechitR", &(m_rechitEvent.rechitR));
            rechitTree->SetBranchAddress("rechitPhi", &(m_rechitEvent.rechitPhi));
            rechitTree->SetBranchAddress("rechitClusterSize", &(m_rechitEvent.rechitClusterSize));
            rechitTree->SetBranchAddress("rechitClusterSizeX", &(m_rechitEvent.rechitClusterSizeX));
            rechitTree->SetBranchAddress("rechitClusterSizeY", &(m_rechitEvent.rechitClusterSizeY));
            rechitTree->SetBranchAddress("rechitCharge", &(m_rechitEvent.rechitCharge));
            rechitTree->SetBranchAddress("rechitTime", &(m_rechitEvent.rechitTime));
        }

        RechitEvent *readEvent(int eventIndex) {
            rechitTree->GetEntry(eventIndex);
            return &m_rechitEvent;
        }

        int getEntries() {
            return rechitTree->GetEntries();
        }

    private:

        TFile *rechitFile;
        TTree *rechitTree;

        RechitEvent m_rechitEvent;

};

class TrackFileWriter {

    public:

        TrackFileWriter(std::string ofile, std::shared_ptr<SetupGeometry> geometry) {
            m_setupGeometry = geometry;

            trackFile = new TFile(ofile.c_str(), "RECREATE", "Track file");
            trackTree = new TTree("trackTree", "trackTree");

            trackTree->Branch("orbitNumber", &m_trackEvent.orbitNumber);
            trackTree->Branch("bunchCounter", &m_trackEvent.bunchCounter);
            trackTree->Branch("eventCounter", &m_trackEvent.eventCounter);

            trackTree->Branch("rechitChamber", &m_trackEvent.rechitChamber);
            trackTree->Branch("rechitEta", &m_trackEvent.rechitEta);
            trackTree->Branch("rechitLocalX", &m_trackEvent.rechitLocalX);
            trackTree->Branch("rechitLocalY", &m_trackEvent.rechitLocalY);
            trackTree->Branch("rechitR", &m_trackEvent.rechitR);
            trackTree->Branch("rechitPhi", &m_trackEvent.rechitPhi);
            trackTree->Branch("rechitErrorX", &m_trackEvent.rechitErrorX);
            trackTree->Branch("rechitErrorY", &m_trackEvent.rechitErrorY);
            trackTree->Branch("rechitGlobalX", &m_trackEvent.rechitGlobalX);
            trackTree->Branch("rechitGlobalY", &m_trackEvent.rechitGlobalY);
            trackTree->Branch("rechitErrorGlobalX", &m_trackEvent.rechitErrorGlobalX);
            trackTree->Branch("rechitErrorGlobalY", &m_trackEvent.rechitErrorGlobalY);
            trackTree->Branch("rechitClusterSize", &m_trackEvent.rechitClusterSize);
            trackTree->Branch("rechitClusterSizeX", &m_trackEvent.rechitClusterSizeX);
            trackTree->Branch("rechitClusterSizeY", &m_trackEvent.rechitClusterSizeY);
            trackTree->Branch("rechitCharge", &m_trackEvent.rechitCharge);
            trackTree->Branch("rechitTime", &m_trackEvent.rechitTime);

            trackTree->Branch("partialTrackChamber", &m_trackEvent.partialTrackChamber);
            trackTree->Branch("partialTrackChi2", &m_trackEvent.partialTrackChi2);
            trackTree->Branch("partialTrackCovarianceX", &m_trackEvent.partialTrackCovarianceX);
            trackTree->Branch("partialTrackCovarianceY", &m_trackEvent.partialTrackCovarianceY);
            trackTree->Branch("partialTrackSlopeX", &m_trackEvent.partialTrackSlopeX);
            trackTree->Branch("partialTrackSlopeY", &m_trackEvent.partialTrackSlopeY);
            trackTree->Branch("partialTrackInterceptX", &m_trackEvent.partialTrackInterceptX);
            trackTree->Branch("partialTrackInterceptY", &m_trackEvent.partialTrackInterceptY);
            trackTree->Branch("partialProphitEta", &m_trackEvent.partialProphitEta);
            trackTree->Branch("partialProphitGlobalX", &m_trackEvent.partialProphitGlobalX);
            trackTree->Branch("partialProphitGlobalY", &m_trackEvent.partialProphitGlobalY);
            trackTree->Branch("partialProphitErrorX", &m_trackEvent.partialProphitErrorX);
            trackTree->Branch("partialProphitErrorY", &m_trackEvent.partialProphitErrorY);
            trackTree->Branch("partialProphitLocalX", &m_trackEvent.partialProphitLocalX);
            trackTree->Branch("partialProphitLocalY", &m_trackEvent.partialProphitLocalY);
            trackTree->Branch("partialProphitR", &m_trackEvent.partialProphitR);
            trackTree->Branch("partialProphitPhi", &m_trackEvent.partialProphitPhi);

            trackTree->Branch("trackChi2", &m_trackEvent.trackChi2);
            trackTree->Branch("trackCovarianceX", &m_trackEvent.trackCovarianceX);
            trackTree->Branch("trackCovarianceY", &m_trackEvent.trackCovarianceY);
            trackTree->Branch("trackSlopeX", &m_trackEvent.trackSlopeX);
            trackTree->Branch("trackSlopeY", &m_trackEvent.trackSlopeY);
            trackTree->Branch("trackInterceptX", &m_trackEvent.trackInterceptX);
            trackTree->Branch("trackInterceptY", &m_trackEvent.trackInterceptY);

            trackTree->Branch("allChi2", &m_trackEvent.allChi2);
            trackTree->Branch("prophitChamber", &m_trackEvent.prophitChamber);
            trackTree->Branch("prophitEta", &m_trackEvent.prophitEta);
            trackTree->Branch("prophitGlobalX", &m_trackEvent.prophitGlobalX);
            trackTree->Branch("prophitGlobalY", &m_trackEvent.prophitGlobalY);
            trackTree->Branch("prophitErrorX", &m_trackEvent.prophitErrorX);
            trackTree->Branch("prophitErrorY", &m_trackEvent.prophitErrorY);
            trackTree->Branch("prophitLocalX", &m_trackEvent.prophitLocalX);
            trackTree->Branch("prophitLocalY", &m_trackEvent.prophitLocalY);
            trackTree->Branch("prophitR", &m_trackEvent.prophitR);
            trackTree->Branch("prophitPhi", &m_trackEvent.prophitPhi);

        }

        ~TrackFileWriter() {
            trackTree->Write();
            trackFile->Close();
        }

        TrackEvent *fromRechit(RechitEvent *rechitEvent) {
            /**
             * Re-use the same track event every time:
             */
            m_trackEvent.clear();

            /**
             * Copy rechit event counters to track event:
             */
            m_trackEvent.eventCounter = rechitEvent->eventCounter;
            m_trackEvent.orbitNumber = rechitEvent->orbitNumber;
            m_trackEvent.bunchCounter = rechitEvent->bunchCounter;

            nrechits = rechitEvent->rechitChamber->size();

            /**
             * Copy all rechit variables to track event:
             */
            for (int irechit=0; irechit<nrechits; irechit++) {
                chamber = rechitEvent->rechitChamber->at(irechit);
                hit = Hit::fromLocal(m_setupGeometry->getChamber(chamber),
                        rechitEvent->rechitX->at(irechit), rechitEvent->rechitY->at(irechit),
                        rechitEvent->rechitErrorX->at(irechit), rechitEvent->rechitErrorY->at(irechit), 0.
                        );
                m_trackEvent.rechitChamber->push_back(chamber);
                m_trackEvent.rechitEta->push_back(rechitEvent->rechitEta->at(irechit));
                m_trackEvent.rechitLocalX->push_back(rechitEvent->rechitX->at(irechit));
                m_trackEvent.rechitLocalY->push_back(rechitEvent->rechitY->at(irechit));
                m_trackEvent.rechitR->push_back(hit.getLocalR());
                m_trackEvent.rechitPhi->push_back(hit.getLocalPhi());
                m_trackEvent.rechitGlobalX->push_back(hit.getGlobalX());
                m_trackEvent.rechitGlobalY->push_back(hit.getGlobalY());
                m_trackEvent.rechitErrorX->push_back(rechitEvent->rechitErrorX->at(irechit));
                m_trackEvent.rechitErrorY->push_back(rechitEvent->rechitErrorY->at(irechit));
                m_trackEvent.rechitErrorGlobalX->push_back(hit.getErrorGlobalX());
                m_trackEvent.rechitErrorGlobalY->push_back(hit.getErrorGlobalY());
                m_trackEvent.rechitClusterSize->push_back(rechitEvent->rechitClusterSize->at(irechit));
                m_trackEvent.rechitClusterSizeX->push_back(rechitEvent->rechitClusterSizeX->at(irechit));
                m_trackEvent.rechitClusterSizeY->push_back(rechitEvent->rechitClusterSizeY->at(irechit));
                m_trackEvent.rechitCharge->push_back(rechitEvent->rechitCharge->at(irechit));
                m_trackEvent.rechitTime->push_back(rechitEvent->rechitTime->at(irechit));

                /**
                 * In addition to copying rechit variables, also calculate rechit layer.
                 * This is determined by the setup geometry and the chamber ID
                 * and is used to create tracks
                 * but is not saved in a branch
                 */
                m_trackEvent.rechitLayer->push_back(m_setupGeometry->getLayer(chamber));

                if (logging::verbose) {
                    fmt::print("  {:4d}. Layer {}, ", irechit, m_trackEvent.rechitLayer->back());
                    std::cout << "chamber " << chamber << ", ";
                    std::cout << "eta " << m_trackEvent.rechitEta->back() << ", ";
                    std::cout << "global carthesian (" << m_trackEvent.rechitGlobalX->back() << "," << m_trackEvent.rechitGlobalY->back() << "), ";
                    std::cout << "local carthesian (" << hit.getLocalX() << "," << hit.getLocalY() << "), ";
                    std::cout << "local polar R=" << hit.getLocalR() << ", phi=" << hit.getLocalPhi() << ", ";
                    std::cout << "charge " << m_trackEvent.rechitCharge->back() << ", ";
                    std::cout << "time " << m_trackEvent.rechitTime->back();
                    std::cout << std::endl;
                }
            }

            /**
             * Do tracking according to the track selection method in geometry.
             * If the algorithm cannot give a good track,
             * flag the event as invalid so it will not be saved.
             */
            try {
                m_setupGeometry->getTrackingAlgorithm()->findTracks(&m_trackEvent);
            } catch (no_track_error& e) {
                if (logging::verbose) {
                    fmt::print("    Could not build track: \"{}\". This event will be skipped\n", e.what());
                }
                m_trackEvent.setValid(false);
            }

            return &m_trackEvent;
        }

        void writeEvent() {
            trackTree->Fill();
        }

    private:

        std::shared_ptr<SetupGeometry> m_setupGeometry;
        TFile *trackFile;
        TTree *trackTree;

        TrackEvent m_trackEvent;

        /**
         * Support variables:
         * - number of rechits in each event
         * - chamber ID
         * - layer ID
         * - rechit, hit and track objects
         */
        int nrechits;
        int chamber;
        int layer;
        Rechit rechit;
        Hit hit;
        Track track;

        /**
         * Support variables to be later moved to separate classes
         * - rechit positions and errors
         * - prophits positions and errors
         */
        double rechitX, rechitY;
        double rechitXError, rechitYError;
        double prophitX, prophitY;
        double prophitXError, prophitYError;
};

int main (int argc, char** argv) {

    std::string rechit_filename, track_filename;
    uint64_t nentries{0};
    std::string geometry_name = "";

    CLI::App app{"Convert SRS output file to digi"};
    app.add_option("input", rechit_filename, "Rechit input ROOT file path")->required();
    app.add_option("output", track_filename, "Track output file path")->required();
    app.add_option("--geometry", geometry_name, "Geometry name");
    app.add_option("-n,--events", nentries, "Maximum number of events to read");
    app.add_flag("-v,--verbose", logging::verbose, "Verbose mode");
    try {
        app.parse(argc, argv);
    } catch (const CLI::ParseError &e) {
        return app.exit(e);
    }

    /* Define detector geometries */
    std::string geometryBaseDir = std::string(std::getenv("MPGD_ANALYSIS_HOME"))+"/geometry/setups/";
    std::shared_ptr<SetupGeometry> setupGeometry{new SetupGeometry(geometryBaseDir+geometry_name+".xml")};

    RechitFileReader reader{rechit_filename};
    TrackFileWriter writer{track_filename, setupGeometry};

    /**
     * Read rechit event, turn it to tracks and save it:
     */
    if (nentries==0) {
        nentries = reader.getEntries();
    }
    RechitEvent *rechitEvent;
    TrackEvent *trackEvent;

    progressbar bar(nentries);
    signal(SIGINT, interruptHandler);
    fmt::print("Processing {} events...\n", nentries); 
    int n_valid_events{0};
    for (int nentry=0; (!isInterrupted) && nentry<nentries; ++nentry) {

        if (logging::verbose) {
            fmt::print("Event {}/{}\n", nentry, nentries);
        } else {
            bar.update();
        }

        rechitEvent = reader.readEvent(nentry);
        if (logging::verbose) {
            rechitEvent->print();
        }
        trackEvent = writer.fromRechit(rechitEvent);
        if (logging::verbose) {
            trackEvent->print();
        }
        if (trackEvent->isValid()) {
            writer.writeEvent();
            n_valid_events++;
        }
    }

    std::cout << std::endl;
    fmt::print("{} valid track events found.\n", n_valid_events); 
    fmt::print("Output file saved to {}\n", track_filename);
}
