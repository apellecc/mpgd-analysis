set(HEADER_FILES
    ../include
    ../include/tracking
    ../include/tracking/algorithms
    ../extern
    ../extern/fmt)

# Compile unpacker only if ZSTD exists:
if (USE_ZSTD)
    add_executable(raw-to-digi raw-to-digi.cc)
    target_include_directories(raw-to-digi PRIVATE ${HEADER_FILES})
    target_link_libraries(raw-to-digi reco_library)
    target_link_libraries(raw-to-digi ${ROOT_LIBRARIES})
    target_link_libraries(raw-to-digi ${ZSTD_LIBRARY})
endif()

add_executable(apv-to-digi apv-to-digi.cc)
target_include_directories(apv-to-digi PRIVATE ${HEADER_FILES})
target_link_libraries(apv-to-digi reco_library)
target_link_libraries(apv-to-digi ${ROOT_LIBRARIES})

add_executable(digi-to-rechit digi-to-rechit.cc)
target_include_directories(digi-to-rechit PRIVATE ${HEADER_FILES})
target_link_libraries(digi-to-rechit reco_library)
target_link_libraries(digi-to-rechit ${ROOT_LIBRARIES})

add_executable(rechit-to-track rechit-to-track.cc)
target_include_directories(rechit-to-track PRIVATE ${HEADER_FILES})
target_link_libraries(rechit-to-track reco_library)
target_link_libraries(rechit-to-track ${ROOT_LIBRARIES})

install(TARGETS raw-to-digi RUNTIME DESTINATION bin)
install(TARGETS apv-to-digi RUNTIME DESTINATION bin)
install(TARGETS digi-to-rechit RUNTIME DESTINATION bin)
install(TARGETS rechit-to-track RUNTIME DESTINATION bin)
