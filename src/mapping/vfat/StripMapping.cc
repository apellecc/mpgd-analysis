#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <iterator>
#include <algorithm>

#include "mapping/vfat/StripMapping.h"
#include "DataFrame.h"

StripMapping::StripMapping(std::string mappingFilePath) : Mapping{mappingFilePath} {
    DataFrame mappingDataFrame = DataFrame::fromCsv(getMappingFilePath());
    int vfat, channel;

    /**
     * Read mapping files, then fill mapping maps
     */
    for (int irow=0; irow<mappingDataFrame.getNRows(); irow++) {
        vfat = std::stoi(mappingDataFrame.getElement("vfat", irow));
        channel = std::stoi(mappingDataFrame.getElement("channel", irow));

        VFATChannelTuple p{vfat, channel};
        toEta[p] = std::stoi(mappingDataFrame.getElement("eta", irow));
        toStrip[p] = std::stoi(mappingDataFrame.getElement("strip", irow));
    }
}

void StripMapping::print() {
    std::cout << "vfat\tchannel\teta\tstrip" << std::endl;
    VFATChannelTuple p;
    int lineIndex = 0;
    int lastLine = toEta.size();
    for (auto mapIterator=toEta.begin(); mapIterator!=toEta.end(); mapIterator++) {
        p = mapIterator->first;
        if ((lineIndex<MAPPING_PRINT_LINES)||(lastLine-lineIndex<MAPPING_PRINT_LINES)) {
            std::cout << std::get<0>(p) << "\t" << std::get<1>(p) << "\t" << toEta[p] << "\t" << toStrip[p] << std::endl;
        } else if (lineIndex==MAPPING_PRINT_LINES) {
            std::cout << "..." << std::endl;
        }
        lineIndex++;
    }
    std::cout << std::endl;
}

