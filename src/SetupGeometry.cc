#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <stdexcept>

#include <fmt/core.h>

#include "SetupGeometry.h"
#include "TrackingAlgorithm.h"
#include "tinyxml2.h"

SetupGeometry::SetupGeometry(std::string geometryFile) {

    tinyxml2::XMLDocument setupGeometryDoc;
    if (setupGeometryDoc.LoadFile(geometryFile.c_str())) {
        throw std::invalid_argument(fmt::format("Could not open setup geometry file {}. Does the file exist?", geometryFile));
    }

    tinyxml2::XMLElement *setupElement = setupGeometryDoc.FirstChildElement("setup");

    const char *setupLabel;
    setupElement->QueryStringAttribute("label", &setupLabel);
    std::cout << "Setup name: " << setupLabel << std::endl;

    /**
     * Determine which tracking algorithm to use
     * and instantiate the corresponding TrackingAlgorithm subclass:
     */
    tinyxml2::XMLElement *trackingSetupElement = setupElement->FirstChildElement("tracking");
    if (!trackingSetupElement) {
        throw std::invalid_argument(fmt::format("Could not find a tracking configuration in the geometry file"));
    }
    const std::string trackSelection = trackingSetupElement->FirstChildElement("trackSelection")->GetText();
    TrackingFactory trackingFactory;
    std::cout << "Available tracking algorithms: " << trackingFactory << std::endl;;
    m_trackingAlgorithm = trackingFactory.make(trackSelection);
    m_trackingAlgorithm->setGeometry(this);
    std::cout << "Track selection is type " << trackSelection << std::endl;

    tinyxml2::XMLElement *detectorElement = setupElement->FirstChildElement("detector");
    while (detectorElement) {
        const char *chamberLabel;
        int detectorID;
        bool isTracker = false;
        const char *readoutType; // strips or pads
        double baseSmall, baseLarge, height;
        int nEta, nReadoutElements;
        double x, y, z, phi_z, reflection;

        detectorElement->QueryStringAttribute("label", &chamberLabel);
        detectorElement->QueryBoolAttribute("tracker", &isTracker);
        detectorElement->QueryIntAttribute("id", &detectorID);

        const char *geometryName = detectorElement->FirstChildElement("geometry")->GetText();
        // Read detector geometry from corresponding file:
        std::string geometryBaseDir = std::string(std::getenv("MPGD_ANALYSIS_HOME"))+"/geometry/detectors/";
        std::string detectorGeometryPath = geometryBaseDir+std::string(geometryName)+".xml";
        tinyxml2::XMLDocument detectorGeometryDoc;
        if (detectorGeometryDoc.LoadFile(detectorGeometryPath.c_str())) {
            throw std::invalid_argument("Could not open detector geometry file " + detectorGeometryPath + ". Does the file exist?");
        }
        tinyxml2::XMLElement *geometryElement = detectorGeometryDoc.FirstChildElement("detectorGeometry")->FirstChildElement("geometry");
        geometryElement->FirstChildElement("baseSmall")->QueryDoubleText(&baseSmall);
        geometryElement->FirstChildElement("baseLarge")->QueryDoubleText(&baseLarge);
        geometryElement->FirstChildElement("height")->QueryDoubleText(&height);

        tinyxml2::XMLElement *readoutElement = detectorGeometryDoc.FirstChildElement("detectorGeometry")->FirstChildElement("readout");
        readoutElement->QueryStringAttribute("type", &readoutType);
        readoutElement->FirstChildElement("nEta")->QueryIntText(&nEta);
        if (std::string(readoutType)=="strips") {
            readoutElement->FirstChildElement("nStrips")->QueryIntText(&nReadoutElements);
        } else if (std::string(readoutType)=="pads") {
            readoutElement->FirstChildElement("nPads")->QueryIntText(&nReadoutElements);
        }

        // Read alignment from detector geometry file:
        tinyxml2::XMLElement *alignmentElement = detectorElement->FirstChildElement("alignment");
        alignmentElement->FirstChildElement("x")->QueryDoubleText(&x);
        alignmentElement->FirstChildElement("y")->QueryDoubleText(&y);
        alignmentElement->FirstChildElement("z")->QueryDoubleText(&z);
        alignmentElement->FirstChildElement("phi_z")->QueryDoubleText(&phi_z);
        auto reflectionElement = alignmentElement->FirstChildElement("reflection");
        if (reflectionElement) {
            reflectionElement->QueryDoubleText(&reflection);
        } else {
            reflection = 1.;
        }

        DetectorGeometry detector(
            detectorID, baseSmall, baseLarge, height, nEta, readoutType, nReadoutElements
        );
        detector.setPosition(
            x, y, z, phi_z, reflection
        );
        detectorMap[detector.getChamber()] = detector;
        if (isTracker) trackerChambers.push_back(detector.getChamber());
        detectorMap[detector.getChamber()].print();

        // Go on to next detector
        detectorElement = detectorElement->NextSiblingElement("detector");
    }

    /* Group together detectors by equal z */
    for (auto detectorPair:detectorMap) {
        double z = detectorPair.second.getPositionZ();
        if (!layerMap.count(z)) {
            layerMap[z] = std::vector<int>();
        }
        layerMap.at(z).push_back(detectorPair.first);
    }
    std::cout << "-------------------------------------------" << std::endl;
    std::cout << "List of detectors at same z position:" << std::endl;
    int layerIndex = 0;
    for (auto layerPair:layerMap) {
        layers.push_back(DetectorLayer(layerPair.first));
        std::cout << "    Detectors at z position " << layerPair.first << ": ";
        for (auto d:layerPair.second) {
            layers.back().addChamber(d);
            layers.back().setTracking(layers.back().isTracking()||isTrackingChamber(d));
            detectorToLayerMap[d] = layerIndex;
            std::cout << d << " ";
        }
        std::cout << "is tracking: " << (layers.back().isTracking() ? "true" : "false");
        std::cout << std::endl;
        layerIndex++;
    }
    std::cout << "Map of detector layers: ";
    for (auto detectorLayerPair:detectorToLayerMap) {
        std::cout << "{" << detectorLayerPair.first << ":" << getLayer(detectorLayerPair.first) << "} ";
    }
    std::cout << std::endl;
    std::cout << "-------------------------------------------" << std::endl;
}

bool SetupGeometry::isTrackingChamber(int chamber) {
    return std::count(trackerChambers.begin(), trackerChambers.end(), chamber)>0;
}

bool SetupGeometry::isTrackingLayer(int layer) {
    return layers[layer].isTracking();
}

DetectorGeometry *SetupGeometry::getChamber(int chamber) {
    if (detectorMap.count(chamber)>0) {
        return &detectorMap.at(chamber);
    } else {
        throw std::invalid_argument(std::string("The detector number " + std::to_string(chamber) + " does not exist or has no mapping."));
    }
}

int SetupGeometry::getLayer(int chamber) {
    return detectorToLayerMap[chamber];
}

std::shared_ptr<TrackingAlgorithm> SetupGeometry::getTrackingAlgorithm() {
    return m_trackingAlgorithm;
}
