#include <math.h>

#include "Hit.h"
#include "DetectorGeometry.h"

Hit::Hit(double x, double y, double z, double errX, double errY, double errZ) {
    m_globalPosition[0] = x;
    m_globalPosition[1] = y;
    m_globalPosition[2] = z;
    m_errGlobalPosition[0] = errX;
    m_errGlobalPosition[1] = errY;
    m_errGlobalPosition[2] = errZ;
}

Hit::Hit(DetectorGeometry *detector, double x, double y, double z, double errX, double errY, double errZ) : Hit(x, y, z, errX, errY, errZ) {
    setDetector(detector);
}

Hit Hit::fromLocal(DetectorGeometry *detector, double localX, double localY, double errorX, double errorY, double errorZ) {
    // std::cout << "              ######### DEBUGGING INFO #########" << std::endl;
    // std::cout << "              local\t\tdet\t\tglobal" << std::endl;
    // std::cout << "              " << localX << "\t\t" << detector->getPositionX() << "\t\t" << localX+detector->getPositionX() << std::endl;
    // std::cout << "              " << localY << "\t\t" << detector->getPositionY() << "\t\t" << localY+detector->getPositionY() << std::endl;
    // std::cout << "              " << detector->getPositionZ() << std::endl;
    // std::cout << "              ######### END DEBUGGING #########" << std::endl;

    // calculate global coordinates:
    double c = cos(detector->getTheta());
    double s = sin(detector->getTheta());
    double globalX = detector->getPositionX() + localX*detector->getReflection()*c - localY*s;
    double globalY = detector->getPositionY() + localX*detector->getReflection()*s + localY*c;
    double errorGlobalX = sqrt(pow(c*errorX,2) + pow(s*errorY,2)); 
    double errorGlobalY = sqrt(pow(s*errorX,2) + pow(c*errorY,2));

    return Hit(
            detector,
            globalX, globalY, detector->getPositionZ(),
            errorGlobalX, errorGlobalY, errorZ
            );
}

void Hit::setDetector(DetectorGeometry *detector) {
    m_detector = detector;
    // calculate local coordinates:
    double x = m_globalPosition[0] - detector->getPositionX();
    double y = m_globalPosition[1] - detector->getPositionY();
    double c = cos(detector->getTheta());
    double s = sin(detector->getTheta());
    m_localPosition[0] = (x*c + y*s) / detector->getReflection();
    m_localPosition[1] = (-x*s + y*c) / detector->getReflection();
    m_errLocalPosition[0] = sqrt(pow(x*c,2) + pow(y*s,2));
    m_errLocalPosition[1] = sqrt(pow(x*s,2) + pow(y*c,2));
    m_localR = sqrt( pow(m_localPosition[0],2) + pow(m_localPosition[1]-detector->getOriginY(),2) );
    m_localPhi = atan( m_localPosition[0]/(m_localPosition[1]-detector->getOriginY()) );
}

int Hit::getChamber() {
    return m_detector->getChamber();
}

int Hit::getEta() {
    // calculate eta partition of hit from detector geometry
    return m_detector->getNEta() - floor((getLocalY()+0.5*m_detector->getHeight())/m_detector->getEtaHeight());
}

bool Hit::isContained() {
    return (abs(getLocalY())<0.5*m_detector->getHeight()) 
        && (abs(getLocalX())<0.5*m_detector->getWidth(getLocalY()));
}
