#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <functional>

#include "ClusterPad.h"
#include "DigiPad.h"

bool ClusterPad::isNeighbour(DigiPad otherDigi) {
    if ( (m_chamber != otherDigi.getChamber()) ) return false;
    for (DigiPad digi: m_digis) {
        if (digi.isNeighbour(otherDigi)) return true;
    }
    return false;
}

void ClusterPad::extend(DigiPad digi) {
    m_digis.push_back(digi);
}

double ClusterPad::getCenterX() {
    /* Return geometrical center */
    double center = 0, charge = 0;
    for (auto digi: m_digis) {
        center += ((double) digi.getPadX()) * ((double) digi.getCharge());
        charge += (double) digi.getCharge();
    }
    return center / charge;
}

double ClusterPad::getCenterY() {
    /* Return geometrical center */
    double center = 0, charge = 0;
    for (auto digi: m_digis) {
        center += ((double) digi.getPadY()) * ((double) digi.getCharge());
        charge += (double) digi.getCharge();
    }
    return center / charge;
}

int ClusterPad::getCharge() {
    int charge = 0;
    for (auto digi: m_digis) {
        charge += digi.getCharge();
    }
    return charge;
}

double ClusterPad::getTime() {
    double time = 0;
    for (auto digi: m_digis) {
        time += (double) digi.getTime();
    }
    return time / m_digis.size();
}

double ClusterPad::getSizeX() {
    int minX = m_digis[0].getPadX(), maxX = 0;
    for (auto digi:m_digis) {
        if (digi.getPadX() < minX) {
            minX = digi.getPadX();
        }
        if (digi.getPadX() > maxX) {
            maxX = digi.getPadX();
        }
    }
    return static_cast<double>(maxX - minX + 1);
}

double ClusterPad::getSizeY() {
    int minY = m_digis[0].getPadY(), maxY = 0;
    for (auto digi:m_digis) {
        if (digi.getPadY() < minY) {
            minY = digi.getPadY();
        }
        if (digi.getPadY() > maxY) {
            maxY = digi.getPadY();
        }
    }
    return static_cast<double>(maxY - minY + 1);
}

std::vector<ClusterPad> ClusterPad::fromDigis(std::vector<DigiPad> digis) {
    auto selectPadsId = [](DigiPad digiSeed, std::vector<DigiPad> neighbourPad, std::vector<unsigned int> indexPad){

        std::vector<unsigned int> goodIndex;

        double seedCharge = digiSeed.getCharge();
        //std::cout<<"<<SELECTION>> seed: "<<seedCharge<<std::endl;
        for(unsigned int i=0;i<neighbourPad.size();i++){
            if (neighbourPad[i].getCharge()/seedCharge > 0.9) goodIndex.push_back(indexPad[i]);
            //std::cout<<"<<SELECTION>>    neighbourPad[i].getCharge()/seed: "<<neighbourPad[i].getCharge()/seedCharge<<std::endl;
        }

        return goodIndex;

    };

    std::vector<ClusterPad> clusters;
    std::sort(digis.begin(), digis.end(), std::greater<DigiPad>());
    int chamber;

    while (digis.size()>0) {


        DigiPad digiSeed = digis[0];
        chamber = digiSeed.getChamber();

        // Use first digi as seed for "proto-cluster" of size 1:
        ClusterPad cluster = ClusterPad(chamber, digiSeed);

        //std::cout<<"###################################################"<<std::endl;
        std::vector<DigiPad> neighbourPad;
        std::vector<unsigned int> indexPad;
        for (unsigned int i=0; i<digis.size();i++){
            if (digis[i].getChamber()==chamber) {
                if(digiSeed.isNeighbour(digis[i])){
                    neighbourPad.push_back(digis[i]);
                    //std::cout<<"GoodIndex: "<<i<<std::endl;
                    indexPad.push_back(i);
                }
            }
        }
        //std::cout<<"SizeNeighbourPad: "<<neighbourPad.size()<<std::endl;
        std::vector<unsigned int> goodIndex = selectPadsId(digiSeed,neighbourPad,indexPad);
        //std::cout<<"SizeGoodIndex: "<<goodIndex.size()<<std::endl;
        //std::cout<<"PositionSeed X:"<<digiSeed.getPadX()<<" Y:"<<digiSeed.getPadY()<<std::endl;
        for (int i=goodIndex.size()-1;i>=0;i--){
            //std::cout<<"digiCharge: "<<digis[goodIndex[i]].getCharge()<<std::endl;
            //std::cout<<"digiIndex: "<<goodIndex[i]<<std::endl;
            //std::cout<<"Position neighbour X:"<<digis[goodIndex[i]].getPadX()<<" Y:"<<digis[goodIndex[i]].getPadY()<<std::endl;
            //std::cout<<"distance: "<<pow(digiSeed.getPadX()-digis[goodIndex[i]].getPadX(),2) + pow(digiSeed.getPadY()-digis[goodIndex[i]].getPadY(),2)<<std::endl;
            cluster.extend(digis[goodIndex[i]]);
            digis.erase(digis.begin()+goodIndex[i]);
        }

        digis.erase(digis.begin());

        clusters.push_back(cluster);
    }
    return clusters;
}
