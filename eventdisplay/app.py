import argparse
import logging
import sys
from xml.dom.minidom import parse, parseString

logging.basicConfig(level=logging.DEBUG)

from flask import Flask
from flask import render_template

import pandas as pd
import uproot 

app = Flask(__name__)

@app.route("/show/<string:run>/<int:event>")
def display(run, event):
    return render_template("index.html", run=run, event=event)

@app.route("/geometry/")
def get_geometry():
    document = parse("../geometry/setups/{}.xml".format(app.config["args"].geometry))
    setup_element = document.documentElement
    detector_elements = setup_element.getElementsByTagName("detector")

    setup_dict = dict()
    for index,detector in enumerate(detector_elements):
        geometry = detector.getElementsByTagName("geometry")[0].firstChild.nodeValue
        detector_dict = {
                "label": detector.getAttribute("label"),
                "chamber": int(detector.getAttribute("id")),
                "geometry": geometry
                }
        detector_alignment = detector.getElementsByTagName("alignment")[0]
        detector_dict["x"] = detector_alignment.getElementsByTagName("x")[0].firstChild.nodeValue
        detector_dict["y"] = detector_alignment.getElementsByTagName("y")[0].firstChild.nodeValue
        detector_dict["z"] = detector_alignment.getElementsByTagName("z")[0].firstChild.nodeValue
        detector_dict["phi_z"] = detector_alignment.getElementsByTagName("phi_z")[0].firstChild.nodeValue

        detector_xml = parse(f"../geometry/detectors/{geometry}.xml")
        geometry_elements = detector_xml.documentElement.getElementsByTagName("geometry")[0]
        readout_elements = detector_xml.documentElement.getElementsByTagName("readout")[0]
        detector_dict["baseSmall"] = geometry_elements.getElementsByTagName("baseSmall")[0].firstChild.nodeValue
        detector_dict["baseLarge"] = geometry_elements.getElementsByTagName("baseLarge")[0].firstChild.nodeValue
        detector_dict["height"] = geometry_elements.getElementsByTagName("height")[0].firstChild.nodeValue
        detector_dict["stripHeight"] = float(detector_dict["height"]) / float(readout_elements.getElementsByTagName("nEta")[0].firstChild.nodeValue)
        setup_dict[detector_dict["chamber"]] = detector_dict

    logging.debug("Setup: %s", setup_dict)
    return setup_dict

@app.route("/get/<string:run>/<int:event>")
def get_track(run, event):
    trackTree = uproot.open("{}/{}.root".format(app.config["args"].trackdir, run))["trackTree"]
    trackParams = trackTree.arrays(
        ["trackSlopeX", "trackSlopeY", "trackInterceptX", "trackInterceptY"],
        entry_start=event, entry_stop=event+1
    )[0]
    rechitParams = trackTree.arrays(
        ["rechitChamber", "rechitGlobalX", "rechitGlobalY", "rechitErrorGlobalX", "rechitErrorGlobalY"],
        entry_start=event, entry_stop=event+1
    )[0]
    n_hits = len(rechitParams["rechitChamber"])
    for i in range(n_hits):
        logging.debug("Chamber: %i, (%1.2f,%1.2f)±(%1.2f,%1.2f)",
                rechitParams["rechitChamber"][i],
                rechitParams["rechitGlobalX"][i],
                rechitParams["rechitGlobalY"][i],
                rechitParams["rechitErrorGlobalX"][i],
                rechitParams["rechitErrorGlobalY"][i],
                )
    trackDict = { key:trackParams[key] for key in trackParams.fields }
    trackDict.update({ key:list(rechitParams[key]) for key in rechitParams.fields })
    return trackDict 

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("trackdir")
    parser.add_argument("geometry")
    args = parser.parse_args()
    app.config["args"] = args
    app.run(debug=True, host="0.0.0.0", port=5050)
