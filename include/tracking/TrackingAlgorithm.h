#include "SetupGeometry.h"
#include "TrackEvent.h"
#include "Track.h"

#ifndef DEF_TRACKING_ALGORITHM
#define DEF_TRACKING_ALGORITHM

/**
 * Exception to be thrown if no good track was found in the event
 * e.g. if there are too many hits to make all possible combinations,
 * or if there are less than three total hits in the tracking detectors
 */
class no_track_error : public std::exception {
    private:
        std::string message;

    public:
        no_track_error(std::string msg) : message(msg){}
        std::string what() {
            return message;
        }
};

/**
 * Find all the partial or full tracks in a TrackEvent
 * and fills the track and prophit variables
 *
 * Available track selection types:
 *
 * CHARGE keeps only the rechit with highest charge per tracking chamber,
 * then uses them to fit tracks
 *
 * CHI2 builds all possible tracks with all possible rechit combinations,
 * then keeps the track with lowest chi2
 * (does not work well with 2 tracking detectors only)
 *
 * PADS is same as CHARGE but uses the non-tracking detectors only
 * and ignores the tracking detectors
 */
class TrackingAlgorithm {
    public: 
        virtual ~TrackingAlgorithm() {};

        /**
         * @brief Extrapolate track to layer and fill prophit variables
         *
         * @param layer: layer on which to extrapolate the track
         * @param isPartialTrack: determine whether the partial track of full track variables have to be filled
         */
        void extrapolateTrack(int layer, bool isPartialTrack);

        /**
         * @brief Finds all tracks in event and fills event variables
         *
         * @param Track event to analyze
         */
        virtual void findTracks(TrackEvent *trackEvent) {
            m_trackEvent = trackEvent;
        }

        /**
         * @param SetupGeometry pointer to store as member
         */
        void setGeometry(SetupGeometry *setupGeometry) {
            m_setupGeometry = setupGeometry;
        }

        /**
         * @brief Get pointer to SetupGeometry
         */
        SetupGeometry *getGeometry() {
            return m_setupGeometry;
        }

        /**
         * @brief Pointer to track event to read rechits and save tracking results:
         */
        TrackEvent *m_trackEvent;

        /**
         * Support variables for hit propagation:
         */
        Hit m_hit;
        Rechit m_rechit;
        Track m_track;
        int m_chamber, m_layer;

    private:

        /**
         * @brief Pointer to setup geometry to be used for track building
         */
        SetupGeometry *m_setupGeometry;

};

#endif
