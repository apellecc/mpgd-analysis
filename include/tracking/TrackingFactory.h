#include <memory>

#ifndef DEF_TRACKING_FACTORY
#define DEF_TRACKING_FACTORY

#include "TrackingAlgorithm.h"
#include "algorithms/BestChi2.h"
#include "algorithms/TMM.h"

/**
 * Creates a TrackingAlgorithm based on the available implementations
 * using factory logic from: https://stackoverflow.com/a/72926855
 */
class TrackingFactory {

    using factoryFunction = std::function<std::shared_ptr<TrackingAlgorithm>()>;

    public:

        /**
         * @brief Registers all available tracking algorithms
         */
        TrackingFactory() {
            m_factoryMap["bestChi2"] = []() { return std::shared_ptr<BestChi2>(new BestChi2()); };
            m_factoryMap["tmmCharge"] = []() { return std::shared_ptr<TMM>(new TMM()); };
        }

        /**
         * @brief Return pointer to tracking algorithm
         *
         * @param Name of algorithm (must be in factory map)
         */
        std::shared_ptr<TrackingAlgorithm> make(std::string name) {
            auto f = m_factoryMap.find(name);
            if (f != m_factoryMap.end()) {
                return f->second();
            } else {
                throw std::invalid_argument(std::string("No tracking algorithm named "+name+" is registered"));
            }
        }

        std::map<std::string, factoryFunction> getFactoryMap() {
            return m_factoryMap;
        }

        friend std::ostream& operator<<(std::ostream& os, const TrackingFactory& f) {
            for (auto p: f.m_factoryMap) {
                os << p.first << " ";
            }
            return os;
        }

    private:
        /**
         * @brief Holds factory function for all tracking algorithms
         */
        std::map<std::string, factoryFunction> m_factoryMap;
};

#endif
