#include "logging.h"

#include "TrackingAlgorithm.h"

void TrackingAlgorithm::extrapolateTrack(int layer, bool isPartialTrack) {
    try {
        m_hit = m_track.propagate(m_setupGeometry, &m_setupGeometry->layers.at(layer));
        m_chamber = m_hit.getChamber();
        if (!isPartialTrack) { // fill track branches
            m_trackEvent->prophitChamber->push_back(m_chamber);
            m_trackEvent->prophitEta->push_back(m_hit.getEta());
            m_trackEvent->prophitGlobalX->push_back(m_hit.getGlobalX());
            m_trackEvent->prophitGlobalY->push_back(m_hit.getGlobalY());
            m_trackEvent->prophitErrorX->push_back(m_hit.getErrorLocalX());
            m_trackEvent->prophitErrorY->push_back(m_hit.getErrorLocalY());
            m_trackEvent->prophitLocalX->push_back(m_hit.getLocalX());
            m_trackEvent->prophitLocalY->push_back(m_hit.getLocalY());
            m_trackEvent->prophitR->push_back(m_hit.getLocalR());
            m_trackEvent->prophitPhi->push_back(m_hit.getLocalPhi());
        } else { // fill partial track branches
            m_trackEvent->partialTrackChamber->push_back(m_chamber);
            m_trackEvent->partialTrackChi2->push_back(m_trackEvent->trackChi2);
            m_trackEvent->partialTrackCovarianceX->push_back(m_trackEvent->trackCovarianceX);
            m_trackEvent->partialTrackCovarianceY->push_back(m_trackEvent->trackCovarianceY);
            m_trackEvent->partialTrackSlopeX->push_back(m_trackEvent->trackSlopeX);
            m_trackEvent->partialTrackSlopeY->push_back(m_trackEvent->trackSlopeY);
            m_trackEvent->partialTrackInterceptX->push_back(m_trackEvent->trackInterceptX);
            m_trackEvent->partialTrackInterceptY->push_back(m_trackEvent->trackInterceptY);
            m_trackEvent->partialProphitEta->push_back(m_hit.getEta());
            m_trackEvent->partialProphitGlobalX->push_back(m_hit.getGlobalX());
            m_trackEvent->partialProphitGlobalY->push_back(m_hit.getGlobalY());
            m_trackEvent->partialProphitErrorX->push_back(m_hit.getErrorLocalX());
            m_trackEvent->partialProphitErrorY->push_back(m_hit.getErrorLocalY());
            m_trackEvent->partialProphitLocalX->push_back(m_hit.getLocalX());
            m_trackEvent->partialProphitLocalY->push_back(m_hit.getLocalY());
            m_trackEvent->partialProphitR->push_back(m_hit.getLocalR());
            m_trackEvent->partialProphitPhi->push_back(m_hit.getLocalPhi());
        }

        if (logging::verbose) {
            std::cout << "      Track extrapolated to layer " << layer;
            std::cout << " (at z = " << m_setupGeometry->getChamber(m_chamber)->getPositionZ() << ") ";
            std::cout << "ended on to chamber " << m_chamber << ": " << std::endl;
            std::cout << "        " << "Track slope (" << m_track.getSlopeX() << "," << m_track.getSlopeY() << ")";
            std::cout << " " << "intercept (" << m_track.getInterceptX() << "," << m_track.getInterceptY() << ")";
            std::cout << std::endl;
            std::cout << "        " << "Prophit " << "eta=" << m_hit.getEta() << ", ";
            std::cout << "global carthesian (" << m_hit.getGlobalX() << "," << m_hit.getGlobalY() << "), ";
            std::cout << "local carthesian (" << m_hit.getLocalX() << "," << m_hit.getLocalY() << "), ";
            std::cout << "polar R=" << m_hit.getLocalR() << ", phi=" << m_hit.getLocalPhi();
            std::cout << std::endl;
        }
    } catch (const std::domain_error& e) {
        // The hit fell outside the chamber, so we skip it:
        if (logging::verbose) {
            fmt::print("      Hit fell outside of chamber borders for layer {}: {}\n", layer, e.what());
        }
    }
}


