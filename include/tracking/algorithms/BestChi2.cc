#include <algorithm>
#include <set>

#include "logging.h"

#include "BestChi2.h"

/**
 * Create all possible tracks with all tracking detectors,
 * excluding layers in argument,
 * then keep only the track with the lowest chi squared 
 */
void BestChi2::createTrackUsingChi2(std::vector<int> excludedLayers) {
    m_trackerTempTracks.clear();
    m_trackEvent->allChi2->clear();
    m_track.clear();

    bool isPartialTrack = excludedLayers.size()>0;
    if (logging::verbose) {
        if (isPartialTrack) {
            std::cout << "    Building track excluding layers ";
            for (auto l:excludedLayers) std::cout << l << " ";
            std::cout << std::endl;
        } else {
            std::cout << "    Building full track" << std::endl;
        }
    }
    // Create array with layers to be used for tracking only:
    std::vector<int> trackingLayers;
    for (int irechit=0; irechit<m_trackEvent->getRechitCount(); irechit++) {
        m_layer = m_trackEvent->rechitLayer->at(irechit);
        if (getGeometry()->isTrackingLayer(m_layer) && std::find(excludedLayers.begin(), excludedLayers.end(), m_layer)==std::end(excludedLayers)) {
            trackingLayers.push_back(m_layer);
        }
    }
    // Create unique array of chambers:
    std::set<int> layersUniqueSet(trackingLayers.begin(), trackingLayers.end());
    std::vector<int> layersUnique(layersUniqueSet.begin(), layersUniqueSet.end());
    int nTrackersInEvent = layersUnique.size();
    if (logging::verbose) {
        std::cout << "      There are " << nTrackersInEvent << " tracking layers in the event: [";
        for (auto l:layersUnique) std::cout << " " << l;
        std::cout << " ]" << std::endl;
    }
    if (nTrackersInEvent < 3) {
        if (logging::verbose) {
            std::cout << "      Not enough tracking layers, skipping event..." << std::endl;
        }
        return;
    }
    // Divide the rechit indices in one vector per tracking layer:
    std::vector<std::vector<int>> rechitIndicesPerLayer(nTrackersInEvent);
    for (int irechit=0; irechit<m_trackEvent->getRechitCount(); irechit++) {
        m_chamber = m_trackEvent->rechitChamber->at(irechit);
        m_layer = m_trackEvent->rechitLayer->at(irechit);
        if (!getGeometry()->isTrackingLayer(m_layer) || std::find(excludedLayers.begin(), excludedLayers.end(), m_layer)!=std::end(excludedLayers)) continue;
        int layerIndex = std::find(layersUnique.begin(), layersUnique.end(), m_layer) - layersUnique.begin();
        rechitIndicesPerLayer[layerIndex].push_back(irechit);
    }
    if (logging::verbose) {
        for (int i=0; i<rechitIndicesPerLayer.size(); i++) {
            std::cout << "      Chamber " << layersUnique[i] << ": ";
            for (auto rechitIndex:rechitIndicesPerLayer[i]) std::cout << rechitIndex << " ";
            std::cout << std::endl;
        }
    }
    // Create an array with an iterator for each chamber:
    std::vector<std::vector<int>::iterator> iterators;
    for (auto it=rechitIndicesPerLayer.begin(); it!=rechitIndicesPerLayer.end(); it++) {
        iterators.push_back(it->begin());
    }

    // Skip event if too many spurious hits:
    int nPossibleTracks = 1;
    for (auto el:rechitIndicesPerLayer) nPossibleTracks *= el.size();
    if (logging::verbose) {
        std::cout << "      There are " << nPossibleTracks << " possible tracks in the event..." << std::endl;
    }
    if (nPossibleTracks < 0 || nPossibleTracks > MAX_ALLOWED_TRACKS) {
        throw no_track_error(
                fmt::format("There are {} possible tracks, but the maximum allowed is {}",
                    nPossibleTracks, MAX_ALLOWED_TRACKS
                    ));
    }

    /* Create all possible rechit combinations per tracking layer
     * using "odometer" method:
     * https://stackoverflow.com/a/1703575
     */
    while (iterators[0] != rechitIndicesPerLayer[0].end()) {
        // build the track with current rechit combination:
        //std::this_thread::sleep_for(std::chrono::milliseconds(50));
        Track testTrack;
        for (auto it:iterators) {
            int rechitIndex = *it;
            m_chamber = m_trackEvent->rechitChamber->at(rechitIndex);
            m_layer = m_trackEvent->rechitLayer->at(rechitIndex);
            m_rechit = Rechit(
                    m_chamber,
                    m_trackEvent->rechitLocalX->at(rechitIndex), m_trackEvent->rechitLocalY->at(rechitIndex),
                    m_trackEvent->rechitErrorX->at(rechitIndex), m_trackEvent->rechitErrorY->at(rechitIndex),
                    m_trackEvent->rechitClusterSize->at(rechitIndex)
                    );
            getGeometry()->getChamber(m_chamber)->mapRechit(&m_rechit); // apply local geometry
            testTrack.addRechit(m_rechit);
        }
        // Build track and append it to the list:
        testTrack.fit();
        if (!testTrack.isValid()) {
            m_trackEvent->trackChi2 = -1.;
        } else {
            m_trackEvent->trackChi2 = testTrack.getChi2Reduced();
        }
        m_trackerTempTracks.push_back(testTrack);
        m_trackEvent->allChi2->push_back(m_trackEvent->trackChi2);
        if (logging::verbose) {
            std::cout << "      Built track with rechit IDs: ";
            for (auto it:iterators) std::cout << *it << " ";
            std::cout << " and chi2 " << m_trackEvent->trackChi2 << std::endl;
        }

        iterators[nTrackersInEvent-1]++; // always scan the least significant vector
        for (int iLayer=nTrackersInEvent-1; (iLayer>0) && (iterators[iLayer]==rechitIndicesPerLayer[iLayer].end()); iLayer--) {
            // if a vector arrived at the end, restart from the beginning
            // and increment the vector one level higher:
            iterators[iLayer] = rechitIndicesPerLayer[iLayer].begin();
            iterators[iLayer-1]++;
        }
    }
    int bestTrackIndex = 0;
    double bestTrackChi2 = m_trackEvent->allChi2->at(0);
    double presentTrackChi2;
    for (int i=0; i<m_trackerTempTracks.size(); i++) {
        //presentTrackChi2 = m_trackerTempTracks.at(i).getChi2ReducedX()+m_trackerTempTracks.at(i).getChi2ReducedY();
        presentTrackChi2 = m_trackEvent->allChi2->at(i);
        if (presentTrackChi2<=bestTrackChi2) {
            bestTrackIndex = i;
            bestTrackChi2 = presentTrackChi2;
        }
    }
    m_track = m_trackerTempTracks.at(bestTrackIndex);
    m_trackEvent->allChi2->erase(m_trackEvent->allChi2->begin() + bestTrackIndex);
    if (logging::verbose) {
        std::cout << "      Found best track at index " << bestTrackIndex;
        std::cout << " with chi2x " << m_track.getChi2X();
        std::cout << " and chi2y " << m_track.getChi2Y() << ". ";
        std::cout << "Slope x " << m_track.getSlopeX() << ", intercept x " << m_track.getInterceptX() << ", ";
        std::cout << "slope y " << m_track.getSlopeY() << ", intercept y " << m_track.getInterceptY();
        std::cout << std::endl;
    }

    if (!m_track.isValid()) {
        m_trackEvent->trackChi2 = -1.;
    } else {
        m_trackEvent->trackChi2 = m_track.getChi2Reduced();
        m_trackEvent->trackCovarianceX = m_track.getCovarianceX();
        m_trackEvent->trackCovarianceY = m_track.getCovarianceY();
        m_trackEvent->trackSlopeX = m_track.getSlopeX();
        m_trackEvent->trackSlopeY = m_track.getSlopeY();
        m_trackEvent->trackInterceptX = m_track.getInterceptX();
        m_trackEvent->trackInterceptY = m_track.getInterceptY();

        /**
         * Extrapolate track to the other detectors:
         */
        for (m_layer=0; m_layer<getGeometry()->layers.size(); m_layer++) {
            /**
             * Only propagate to non-tracking detectors:
             * if this is a partial track, propagate only if it is in the excluded list;
             * if this is a full track, propagate only if it is not a tracking detector.
             */
            if (isPartialTrack && std::find(excludedLayers.begin(), excludedLayers.end(), m_layer)==std::end(excludedLayers)) continue; 
            else if (!isPartialTrack && getGeometry()->isTrackingLayer(m_layer)) continue;
            extrapolateTrack(m_layer, excludedLayers.size()!=0);
        }
    }
}


