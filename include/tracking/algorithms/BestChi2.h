#include "Rechit.h"
#include "TrackingAlgorithm.h"
#include "logging.h"

#ifndef DEF_BEST_CHI2
#define DEF_BEST_CHI2

class BestChi2: public TrackingAlgorithm {
    public: 

        BestChi2() {
            std::cout << "Creating tracking algorithm of subclass bestChi2" << std::endl;
        }

        /**
         * @brief Support function for track reconstruction from rechit variables and extrapolation
         *
         * @param list of IDs of the detectors to be used for the reconstruction
         */
        void createTrackUsingChi2(std::vector<int> excludedLayers);

        /**
         * @brief Find best track in event
         *
         * You need to override this function from the TrackingAlgorithm class
         */
        void findTracks(TrackEvent *trackEvent) {
            /**
             * Call the base class first to ensure the trackEvent is assigned correctly
             */
            TrackingAlgorithm::findTracks(trackEvent);

            for (int irechit=0; irechit<m_trackEvent->getRechitCount(); irechit++) {
                m_chamber = m_trackEvent->rechitChamber->at(irechit);
                m_layer = m_trackEvent->rechitLayer->at(irechit);
            }

            /**
             * Iterate on tracking layers, exclude them
             * and reconstruct track on remaining ones.
             * Extrapolate track on all the excluded layers
             */
            for (int excludedLayer=0; excludedLayer<getGeometry()->layers.size(); excludedLayer++) {
                if (getGeometry()->isTrackingLayer(excludedLayer)) {
                    createTrackUsingChi2(std::vector<int>{excludedLayer});
                }
            }

            /* Create track using all tracking detectors: */
            if (logging::verbose) {
                std::cout << "  #### Extrapolation on non-tracking detectors ####" << std::endl;
            }
            createTrackUsingChi2(std::vector<int>(0));
        }

    private:

        /**
         * Will hold all possible tracks built with all tracker
         * and choose only one at the end:
         */
        std::vector<Track> m_trackerTempTracks; 

        /**
         * Maximum number of possible tracks in the event;
         * if higher, an exception is raised
         */
        const int MAX_ALLOWED_TRACKS{1000};

};

#endif
