#include <algorithm>

#include "Rechit.h"
#include "TrackingAlgorithm.h"
#include "logging.h"

#ifndef DEF_TRACKING_ALGOTIRHM_TMM
#define DEF_TRACKING_ALGOTIRHM_TMM

/**
 * Create track using two TMMs using hit with largest charge per each TMM;
 * only use hit if ratio charge y / charge x is between 3 and 9
 * the layers used will be x1, x2, y1, y2 in order of z position
 */
class TMM: public TrackingAlgorithm {

    private:
        const double lowerChargeRatio{3}, higherChargeRatio{9};

    public: 

        TMM() {
            std::cout << "Creating tracking algorithm of subclass TMM" << std::endl;
        }

        /**
         * @brief Find best track in event
         *
         * You need to override this function from the TrackingAlgorithm class
         */
        void findTracks(TrackEvent *trackEvent) {
            /**
             * Call the base class first to ensure the trackEvent is assigned correctly
             */
            TrackingAlgorithm::findTracks(trackEvent);

            m_track.clear();

            /**
             * Do not fill partial track branches,
             * only build track using highest-charge rechit in each tracking chamber.
             * HACK: works only with 2 TMMs and assumes their order is x1, y1, x2, y2
             */
            std::vector<int> trackingLayers;
            for (int irechit=0; irechit<m_trackEvent->getRechitCount(); irechit++) {
                m_layer = m_trackEvent->rechitLayer->at(irechit);
                if (getGeometry()->isTrackingLayer(m_layer) && (std::find(trackingLayers.begin(), trackingLayers.end(), m_layer)==trackingLayers.end()) ) {
                    trackingLayers.push_back(m_layer);
                }
            }
            std::sort(trackingLayers.begin(), trackingLayers.end());
            if (logging::verbose) {
                std::cout << "  Tracking layers: ";
                for (auto l:trackingLayers) {
                    std::cout << l << " ";
                }
                std::cout << std::endl;
            }

            /**
             * For each tracker, find the rechit with the highest charge
             */
            std::map<int,int> trackingIndices; // holds layer and index of rechit with highest charge 
            std::map<int,int> trackingCharges; // holds layer and charge of rechit with highest charge
            for (int i=0; i<m_trackEvent->rechitChamber->size(); i++) {
                m_layer = m_trackEvent->rechitLayer->at(i);
                // Look for layer among tracking layers:
                auto padLayerIt = std::find(trackingLayers.begin(), trackingLayers.end(), m_layer);
                // Skip hit if the layer is not among the tracking ones:
                if (padLayerIt == trackingLayers.end()) continue;
                int trackingLayerIndex = padLayerIt - trackingLayers.begin();
                // Check first if there is already a "temporary highest" hit in this layer:
                if (trackingCharges.count(m_layer)>0) {
                    // Check if present rechit has larger charge:
                    if (m_trackEvent->rechitCharge->at(i)>trackingCharges.at(m_layer)) {
                        // Update charge and index maps:
                        trackingCharges[m_layer] = m_trackEvent->rechitCharge->at(i);
                        trackingIndices[m_layer] = i;
                    }
                } else {
                    // It is the first occurrence of hit in this layer, so we put it in the map:
                    trackingCharges[m_layer] = m_trackEvent->rechitCharge->at(i);
                    trackingIndices[m_layer] = i;
                }
            }
            if (logging::verbose) {
                std::cout << "    Hits with highest charge: ";
                int ipair = 0;
                for (auto hitPair: trackingIndices) {
                    fmt::print(
                            "{} for layer {} (charge {}){}",
                            hitPair.second, hitPair.first, trackingCharges.at(hitPair.first),
                            (ipair < trackingIndices.size()-1) ? ", " : ""
                            );
                    ipair++;
                }
                std::cout << std::endl;
            }
            /**
             * Check that there is at least one hit per tracking chamber
             */
            if (trackingIndices.size() != getGeometry()->trackerChambers.size()) {
                if (logging::verbose) {
                    std::cout << "    Only " << trackingIndices.size() << " tracking layers gave hits ";
                    std::cout << "and we needed " << trackingLayers.size() << ", skipping event..." << std::endl;
                }
                throw no_track_error(
                        fmt::format("Only {} tracking hits out of {}",
                            trackingIndices.size(), getGeometry()->trackerChambers.size()
                            ));
            }
            /**
             * Check that the hits have a good charge ratio
             */
            double chargeRatio1 = (double) trackingCharges.at(trackingLayers.at(1))/trackingCharges.at(trackingLayers.at(0));
            double chargeRatio2 = (double) trackingCharges.at(trackingLayers.at(3))/trackingCharges.at(trackingLayers.at(2));
            if (std::min(chargeRatio1, chargeRatio2) < lowerChargeRatio || std::max(chargeRatio1, chargeRatio2) > higherChargeRatio) {
                throw no_track_error(
                        fmt::format("Charge ratios {:.2f} and {:.2f} are outside the allowed range ({},{})",
                            chargeRatio1, chargeRatio2, lowerChargeRatio, higherChargeRatio
                            ));
            }

            /**
             * Calculate slope and intercept for the track:
             * slope = (x2-x1) / (z2-z1),
             * intercept = x2 - z2 * (x2-x1) / (z2-z1)
             */
            double x1 = m_trackEvent->rechitGlobalX->at(trackingIndices.at(trackingLayers[0]));
            double y1 = m_trackEvent->rechitGlobalY->at(trackingIndices.at(trackingLayers[1]));
            double x2 = m_trackEvent->rechitGlobalX->at(trackingIndices.at(trackingLayers[2]));
            double y2 = m_trackEvent->rechitGlobalY->at(trackingIndices.at(trackingLayers[3]));
            double z1 = getGeometry()->getChamber(m_trackEvent->rechitChamber->at(trackingIndices.at(trackingLayers[0])))->getPositionZ();
            double z2 = getGeometry()->getChamber(m_trackEvent->rechitChamber->at(trackingIndices.at(trackingLayers[2])))->getPositionZ();
            m_trackEvent->trackSlopeX = (x2-x1) / (z2-z1);
            m_trackEvent->trackSlopeY = (y2-y1) / (z2-z1);
            m_trackEvent->trackInterceptX = x2 - z2 * m_trackEvent->trackSlopeX;
            m_trackEvent->trackInterceptY = y2 - z2 * m_trackEvent->trackSlopeY;
            m_track = Track(m_trackEvent->trackSlopeX, m_trackEvent->trackSlopeY, m_trackEvent->trackInterceptX, m_trackEvent->trackInterceptY);
            if (logging::verbose) {
                std::cout << "    Track seeds: ";
                std::cout << "(" << x1 << "," << y1 << "," << z1 << "), ";
                std::cout << "(" << x2 << "," << y2 << "," << z2 << ")";
                std::cout << std::endl;
                std::cout << "    Track parameters: ";
                std::cout << "slope (" << m_trackEvent->trackSlopeX << "," << m_trackEvent->trackSlopeY << "), ";
                std::cout << "intercept (" << m_trackEvent->trackInterceptX << "," << m_trackEvent->trackInterceptY << ")";
                std::cout << std::endl;
            }

            /**
             * Extrapolate track on the other detectors;
             * we never have partial tracks with this method
             */
            for (m_layer=0; m_layer<getGeometry()->layers.size(); m_layer++) {
                if (getGeometry()->isTrackingLayer(m_layer)) continue;
                extrapolateTrack(m_layer, false);
            }
        }
};

#endif
