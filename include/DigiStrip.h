#include <iostream>

#include "Digi.h"

#ifndef DEF_DIGI_STRIP
#define DEF_DIGI_STRIP

class DigiStrip : public Digi {
    
    public:

        /**
         * Copy constructor
         */
        DigiStrip(DigiStrip *digi) : Digi(digi->getChamber(), digi->getCharge(), digi->getTime()), m_eta{digi->getEta()}, m_strip{digi->getStrip()} {}

        /**
         * Constructor from chamber, eta, strip
         */
        DigiStrip(int chamber, int eta, int strip) : Digi(chamber), m_eta{eta}, m_strip{strip} {}
        DigiStrip(int chamber, int eta, int strip, int charge, int time) : Digi(chamber, charge, time), m_eta{eta}, m_strip{strip} {}

        int getEta() { return m_eta; }
        int getStrip() { return m_strip; }

        std::ostream& operator<<(std::ostream &os) {
            return os << "chamber " << m_chamber << " eta " << m_eta << " strip " << m_strip << " charge " << m_charge << " time " << m_time;
        }

    private:
        int m_eta, m_strip;
};

#endif
