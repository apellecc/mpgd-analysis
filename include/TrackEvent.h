#include <fmt/core.h>

#ifndef DEF_TRACK_EVENT
#define DEF_TRACK_EVENT

class TrackEvent {
    
    public:

        /**
         * Empty constructor
         */
        TrackEvent () {
            rechitLayer = new std::vector<int>();
            rechitChamber = new std::vector<int>();
            rechitEta = new std::vector<double>();
            rechitLocalX = new std::vector<double>();
            rechitLocalY = new std::vector<double>();
            rechitR = new std::vector<double>();
            rechitPhi = new std::vector<double>();
            rechitErrorX = new std::vector<double>();
            rechitErrorY = new std::vector<double>();
            rechitGlobalX = new std::vector<double>();
            rechitGlobalY = new std::vector<double>();
            rechitErrorGlobalX = new std::vector<double>();
            rechitErrorGlobalY = new std::vector<double>();
            rechitClusterSize = new std::vector<double>();
            rechitClusterSizeX = new std::vector<double>();
            rechitClusterSizeY = new std::vector<double>();
            rechitCharge = new std::vector<int>();
            rechitTime = new std::vector<double>();
            partialTrackChamber = new std::vector<int>();
            partialTrackChi2 = new std::vector<double>();
            partialTrackCovarianceX = new std::vector<double>();
            partialTrackCovarianceY = new std::vector<double>();
            partialTrackSlopeX = new std::vector<double>();
            partialTrackSlopeY = new std::vector<double>();
            partialTrackInterceptX = new std::vector<double>();
            partialTrackInterceptY = new std::vector<double>();
            partialProphitEta = new std::vector<double>();
            partialProphitGlobalX = new std::vector<double>();
            partialProphitGlobalY = new std::vector<double>();
            partialProphitErrorX = new std::vector<double>();
            partialProphitErrorY = new std::vector<double>();
            partialProphitLocalX = new std::vector<double>();
            partialProphitLocalY = new std::vector<double>();
            partialProphitR = new std::vector<double>();
            partialProphitPhi = new std::vector<double>();
            allChi2 = new std::vector<double>();
            prophitChamber = new std::vector<int>();
            prophitEta = new std::vector<double>();
            prophitGlobalX = new std::vector<double>();
            prophitGlobalY = new std::vector<double>();
            prophitErrorX = new std::vector<double>();
            prophitErrorY = new std::vector<double>();
            prophitLocalX = new std::vector<double>();
            prophitLocalY = new std::vector<double>();
            prophitR = new std::vector<double>();
            prophitPhi = new std::vector<double>();
        }

        ~TrackEvent() {
            clear();
        }

        void clear() {
            m_valid = true;
            rechitLayer->clear();
            rechitChamber->clear();
            rechitEta->clear();
            rechitLocalX->clear();
            rechitLocalY->clear();
            rechitR->clear();
            rechitPhi->clear();
            rechitErrorX->clear();
            rechitErrorY->clear();
            rechitGlobalX->clear();
            rechitGlobalY->clear();
            rechitErrorGlobalX->clear();
            rechitErrorGlobalY->clear();
            rechitClusterSize->clear();
            rechitClusterSizeX->clear();
            rechitClusterSizeY->clear();
            rechitCharge->clear();
            rechitTime->clear();
            partialTrackChamber->clear();
            partialTrackChi2->clear();
            partialTrackCovarianceX->clear();
            partialTrackCovarianceY->clear();
            partialTrackSlopeX->clear();
            partialTrackSlopeY->clear();
            partialTrackInterceptX->clear();
            partialTrackInterceptY->clear();
            partialProphitEta->clear();
            partialProphitGlobalX->clear();
            partialProphitGlobalY->clear();
            partialProphitErrorX->clear();
            partialProphitErrorY->clear();
            partialProphitLocalX->clear();
            partialProphitLocalY->clear();
            partialProphitR->clear();
            partialProphitPhi->clear();
            allChi2->clear();
            prophitChamber->clear();
            prophitEta->clear();
            prophitGlobalX->clear();
            prophitGlobalY->clear();
            prophitErrorX->clear();
            prophitErrorY->clear();
            prophitLocalX->clear();
            prophitLocalY->clear();
            prophitR->clear();
            prophitPhi->clear();
        }

        /**
         * @return Number of rechits in event
         */
        int getRechitCount() {
            return rechitChamber->size();
        }

        /**
         * @brief Set whether the event is a good track event or not
         */
        void setValid(bool valid) {
            m_valid = valid;
        }

        bool isValid() {
            return m_valid;
        }

        /**
         * Branch variables:
         */
        int orbitNumber, bunchCounter, eventCounter;

        /**
         * Track variables for the output tree
         * Divided in three groups:
         * - rechit variables
         * - partial track variables and related prophits
         * - full track variables and related prophits
         */
        /** Rechits **/
        std::vector<int> *rechitLayer; // support layer, not to be saved in branch
        std::vector<int> *rechitChamber;
        std::vector<double> *rechitEta;
        std::vector<double> *rechitLocalX;
        std::vector<double> *rechitLocalY;
        std::vector<double> *rechitR;
        std::vector<double> *rechitPhi;
        std::vector<double> *rechitErrorX;
        std::vector<double> *rechitErrorY;
        std::vector<double> *rechitGlobalX;
        std::vector<double> *rechitGlobalY;
        std::vector<double> *rechitErrorGlobalX;
        std::vector<double> *rechitErrorGlobalY;
        std::vector<double> *rechitClusterSize;
        std::vector<double> *rechitClusterSizeX;
        std::vector<double> *rechitClusterSizeY;
        std::vector<int> *rechitCharge;
        std::vector<double> *rechitTime;
        /** Partial tracks **/
        std::vector<int> *partialTrackChamber;
        std::vector<double> *partialTrackChi2;
        std::vector<double> *partialTrackCovarianceX, *partialTrackCovarianceY;
        std::vector<double> *partialTrackSlopeX, *partialTrackSlopeY;
        std::vector<double> *partialTrackInterceptX, *partialTrackInterceptY;
        std::vector<double> *partialProphitEta;
        std::vector<double> *partialProphitGlobalX, *partialProphitGlobalY;
        std::vector<double> *partialProphitErrorX, *partialProphitErrorY;
        std::vector<double> *partialProphitLocalX, *partialProphitLocalY;
        std::vector<double> *partialProphitR, *partialProphitPhi;
        /** Tracks built using all tracking detectors **/
        double trackChi2;
        double trackCovarianceX, trackCovarianceY;
        double trackSlopeX, trackSlopeY;
        double trackInterceptX, trackInterceptY;
        std::vector<double> *allChi2;
        std::vector<int> *prophitChamber;
        std::vector<double> *prophitEta;
        std::vector<double> *prophitGlobalX;
        std::vector<double> *prophitGlobalY;
        std::vector<double> *prophitErrorX;
        std::vector<double> *prophitErrorY;
        std::vector<double> *prophitLocalX;
        std::vector<double> *prophitLocalY;
        std::vector<double> *prophitR;
        std::vector<double> *prophitPhi;

        void print() {
            fmt::print("Track event:\n");
            fmt::print("  EC {}, OC {}, BC {}\n",
                    eventCounter, orbitNumber, bunchCounter
                    );
            fmt::print("  Rechits:\n");
            fmt::print("    {:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>9}\t{:>9}\t{:>13}\t{:>13}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\n",
                    "layer", "chamber", "eta",
                    "local x", "local y", "err x", "err y", "r", "phi",
                    "global x", "global y", "err global x", "err global y",
                    "size", "size x", "size y", "charge", "time"
                    );
            for (int i=0; i<rechitChamber->size(); i++) {
                fmt::print("    {:>7}\t{:>7}\t{:>7}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>9.2f}\t{:>9.2f}\t{:>13.2f}\t{:>13.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7d}\t{:>7.2f}\n",
                        rechitLayer->at(i),
                        rechitChamber->at(i),
                        rechitEta->at(i),
                        rechitLocalX->at(i),
                        rechitLocalY->at(i),
                        rechitErrorX->at(i),
                        rechitErrorY->at(i),
                        rechitR->at(i),
                        rechitPhi->at(i),
                        rechitGlobalX->at(i),
                        rechitGlobalY->at(i),
                        rechitErrorGlobalX->at(i),
                        rechitErrorGlobalY->at(i),
                        rechitClusterSize->at(i),
                        rechitClusterSizeX->at(i),
                        rechitClusterSizeY->at(i),
                        rechitCharge->at(i),
                        rechitTime->at(i)
                        );
            }
            fmt::print("  Partial prophits:\n");
            fmt::print("    {:>7}\t{:>7}\t{:>9}\t{:>9}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>13}\t{:>13}\n",
                    "chamber", "eta",
                    "global x", "global y", "err x", "err y",
                    "local x", "local y", "r", "phi",
                    "chi2", "cov x", "cov y",
                    "slope x", "slope y", "intercept x", "intercept y"
                    );
            for (int i=0; i<partialTrackChamber->size(); i++) {
                fmt::print("    {:>7d}\t{:>7.2f}\t{:>9.2f}\t{:>9.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>13.2f}\t{:>13.2f}\n",
                        partialTrackChamber->at(i),
                        partialProphitEta->at(i),
                        partialProphitGlobalX->at(i),
                        partialProphitGlobalY->at(i),
                        partialProphitErrorX->at(i),
                        partialProphitErrorY->at(i),
                        partialProphitLocalX->at(i),
                        partialProphitLocalY->at(i),
                        partialProphitR->at(i),
                        partialProphitPhi->at(i),
                        partialTrackChi2->at(i),
                        partialTrackCovarianceX->at(i),
                        partialTrackCovarianceY->at(i),
                        partialTrackSlopeX->at(i),
                        partialTrackSlopeY->at(i),
                        partialTrackInterceptX->at(i),
                        partialTrackInterceptY->at(i)
                        );
            }
            fmt::print("  Full track:\n");
            fmt::print("    {:>7}\t{:>7}\t{:>7}\t{:>9}\t{:>9}\t{:>13}\t{:>13}\n",
                    "chi2", "cov x", "cov y", "slope x", "slope y", "intercept x", "intercept y"
                    );
            fmt::print("    {:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>9.2f}\t{:>9.2f}\t{:>13.2f}\t{:>13.2f}\n",
                    trackChi2, trackCovarianceX, trackCovarianceY, trackSlopeX, trackSlopeY, trackInterceptX, trackInterceptY
                    );
            fmt::print("  Prophits:\n");
            fmt::print("    {:>7}\t{:>7}\t{:>9}\t{:>9}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\n",
                    "chamber", "eta",
                    "global x", "global y", "err x", "err y",
                    "local x", "local y", "r", "phi"
                    );
            for (int i=0; i<prophitChamber->size(); i++) {
                fmt::print("    {:>7d}\t{:>7.2f}\t{:>9.2f}\t{:>9.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\n",
                        prophitChamber->at(i),
                        prophitEta->at(i),
                        prophitGlobalX->at(i),
                        prophitGlobalY->at(i),
                        prophitErrorX->at(i),
                        prophitErrorY->at(i),
                        prophitLocalX->at(i),
                        prophitLocalY->at(i),
                        prophitR->at(i),
                        prophitPhi->at(i)
                        );
            }
            fmt::print("-----------------------------------------------------\n");
            fmt::print("This event is {}.\n", m_valid ? "valid" : "not valid");
        }

    private:

        bool m_valid{true};
};

#endif
