#include <cstdio>
#include <string>

#ifndef DEF_MAPPING
#define DEF_MAPPING

#define MAPPING_PRINT_LINES 10

class Mapping {
    
    public:

        /**
         * Default constructor
         */
        Mapping() {}

        /**
         * Copy constructor
         */
        Mapping(Mapping *otherMapping) : Mapping(otherMapping->getMappingFilePath()) {}

        /**
         * Constructor from mapping CSV file
         */
	    Mapping(std::string mappingFilePath) : m_mappingFilePath{mappingFilePath} {}

        std::string getMappingFilePath() { return m_mappingFilePath; }

	    virtual void print() = 0;

    protected:

        std::string m_mappingFilePath;
};

#endif
