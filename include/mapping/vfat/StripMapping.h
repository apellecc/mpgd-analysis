#include <cstdio>
#include <string>

#include "mapping/Mapping.h"

#ifndef DEF_VFAT_STRIP_MAPPING
#define DEF_VFAT_STRIP_MAPPING

typedef std::tuple<int, int> VFATChannelTuple;

class StripMapping : public Mapping {
    
    public:
        
        /*
         * Default constructor
         */
        StripMapping() {}

        /**
         * Copy constructor
         */
        StripMapping(StripMapping *otherMapping) : StripMapping(otherMapping->getMappingFilePath()) {}

        /**
         * Constructor from mapping CSV file
         */
	    StripMapping(std::string mappingFile);
	    
        /**
         * Getter methods
         */
        int getEta(VFATChannelTuple p) { return toEta[p]; }
        int getStrip(VFATChannelTuple p) { return toStrip[p]; }

        void print();

    protected:

        std::map<VFATChannelTuple, int> toEta;
        std::map<VFATChannelTuple, int> toStrip;
};

#endif

