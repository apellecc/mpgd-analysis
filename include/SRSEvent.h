#include <vector>
#ifndef DEF_SRS_EVENT
#define DEF_SRS_EVENT

class SRSEvent {
    
    public:

        /**
         * Empty constructor
         */
        SRSEvent () {}

        /**
         * Branch attributes
         */
        std::vector<unsigned int> *srsFec = 0;
        std::vector<unsigned int> *srsChip = 0;
        std::vector<unsigned int> *srsChannel = 0;
        std::vector<short> *maxCharge= 0;
        std::vector<int> *timeMaxCharge= 0;

        void print() {
            std::cout << "SRS event:" << std::endl;
            std::cout << "FEC\tchip\tchannel\tcharge\ttime" << std::endl;
            for (int i=0; i<srsFec->size(); i++) {
                std::cout << srsFec->at(i) << "\t";
                std::cout << srsChip->at(i) << "\t";
                std::cout << srsChannel->at(i) << "\t";
                std::cout << maxCharge->at(i) << "\t";
                std::cout << timeMaxCharge->at(i) << "\t";
                std::cout << std::endl;
            }
            std::cout << "--------------------------------------" << std::endl;
        }
};

#endif
