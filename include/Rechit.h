#include "Cluster.h"

#ifndef DEF_RECHIT
#define DEF_RECHIT

class Rechit {

    public:

        Rechit() {}
        Rechit(int chamber, double x, double y, double errorX, double errorY, double clusterSize);
        Rechit(int chamber, double x, double y, double errorX, double errorY, double clusterSize, int charge, double time) : 
            Rechit(chamber, x, y, errorX, errorY, clusterSize) {
                m_charge = charge;
                m_time = time;
            }

        Rechit(int chamber, double x, double y, double errorX, double errorY, double clusterSizeX, double clusterSizeY, int charge, double time) : 
            Rechit(chamber, x, y, errorX, errorY, 0) {
                m_charge = charge;
                m_time = time;
                m_clusterSizeX = clusterSizeX;
                m_clusterSizeY = clusterSizeY;
            }

        double getX() {return m_x; }
        double getY() {return m_y; }
        double getErrorX() {return m_errorX; }
        double getErrorY() {return m_errorY; }
        double getR() {return m_r; }
        double getPhi() {return m_phi; }
        double getClusterSize() {return m_clusterSize; }
        double getClusterSizeX() {return m_clusterSizeX; }
        double getClusterSizeY() {return m_clusterSizeY; }
        int getChamber() {return m_chamber;}
        int getCharge() {return m_charge;}
        double getTime() {return m_time;}
        
        void setX(double x) { m_x = x; }
        void setY(double y) { m_y = y; }
        void setR(double r) { m_r = r; }
        void setPhi(double phi) { m_phi = phi; }

        void setGlobalPosition(double x, double y, double z, double errorX, double errorY);
        double getGlobalX() { return m_globalX; }
        double getGlobalY() { return m_globalY; }
        double getGlobalZ() { return m_globalZ; }
        double getErrorGlobalX() {return m_errorGlobalX; }
        double getErrorGlobalY() {return m_errorGlobalY; }

        void print() {
            std::cout << m_chamber << ", ";
            std::cout << "(" << m_x << ", " << m_y << ") +/- ";
            std::cout << "(" << m_errorX << ", " << m_errorY << ")" << std::endl;
        }
    
    private:
        double m_x, m_y;
        double m_r, m_phi;
        double m_errorX, m_errorY;
        double m_globalX, m_globalY, m_globalZ;
        double m_errorGlobalX, m_errorGlobalY;

        int m_chamber;
        double m_clusterSize;
        double m_clusterSizeX, m_clusterSizeY;
        int m_charge;
        double m_time;
};

#endif
