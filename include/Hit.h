#ifndef DEF_HIT
#define DEF_HIT

#include "DetectorGeometry.h"

class Hit {

    public:
        Hit() {}
        Hit(double x, double y, double z, double errX, double errY, double errZ);
        Hit(DetectorGeometry *detector, double x, double y, double z, double errX, double errY, double errZ);
        
        static Hit fromLocal(DetectorGeometry *detector, double x, double y, double errX, double errY, double errZ);

        void setDetector(DetectorGeometry *detector);

        double getGlobalX() {return m_globalPosition[0];}
        double getGlobalY() {return m_globalPosition[1];}
        double getGlobalZ() {return m_globalPosition[2];}
        double getErrorGlobalX() {return m_errGlobalPosition[0];}
        double getErrorGlobalY() {return m_errGlobalPosition[1];}
        double getErrorGlobalZ() {return m_errGlobalPosition[2];}
        double getLocalX() {return m_localPosition[0];}
        double getLocalY() {return m_localPosition[1];}
        double getErrorLocalX() {return m_errLocalPosition[0];}
        double getErrorLocalY() {return m_errLocalPosition[0];}
        double getLocalR() {return m_localR;}
        double getLocalPhi() {return m_localPhi;}

        /**
         * Determines if the hit is contained in its chamber surface
         */
        bool isContained();

        int getChamber();
        int getEta();

    private:
        double m_globalPosition[3];
        double m_localPosition[2];
        double m_localR, m_localPhi;
        double m_errGlobalPosition[3];
        double m_errLocalPosition[3];
        DetectorGeometry *m_detector;
};

#endif
