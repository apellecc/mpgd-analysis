#include <fmt/core.h>

#ifndef DEF_RECHIT_EVENT
#define DEF_RECHIT_EVENT

class RechitEvent {
    
    public:

        /**
         * Empty constructor
         */
        RechitEvent () {
            clusterStripChamber = new std::vector<int>();
            clusterStripEta = new std::vector<int>();
            clusterStripCenter = new std::vector<double>();
            clusterStripSize = new std::vector<double>();
            clusterPadChamber = new std::vector<int>();
            clusterPadCenterX = new std::vector<double>();
            clusterPadCenterY = new std::vector<double>();
            clusterPadSizeX = new std::vector<double>();
            clusterPadSizeY = new std::vector<double>();
            rechitChamber = new std::vector<int>();
            rechitEta = new std::vector<int>();
            rechitX = new std::vector<double>();
            rechitY = new std::vector<double>();
            rechitErrorX = new std::vector<double>();
            rechitErrorY = new std::vector<double>();
            rechitR = new std::vector<double>();
            rechitPhi = new std::vector<double>();
            rechitClusterSize = new std::vector<double>();
            rechitClusterSizeX = new std::vector<double>();
            rechitClusterSizeY = new std::vector<double>();
            rechitCharge = new std::vector<int>();
            rechitTime = new std::vector<double>();
        }

        ~RechitEvent() {
            clear();
        }

        void clear() {
            clusterStripChamber->clear();
            clusterStripEta->clear();
            clusterStripCenter->clear();
            clusterStripSize->clear();
            clusterPadChamber->clear();
            clusterPadCenterX->clear();
            clusterPadCenterY->clear();
            clusterPadSizeX->clear();
            clusterPadSizeY->clear();
            rechitChamber->clear();
            rechitEta->clear();
            rechitX->clear();
            rechitY->clear();
            rechitErrorX->clear();
            rechitErrorY->clear();
            rechitR->clear();
            rechitPhi->clear();
            rechitClusterSize->clear();
            rechitClusterSizeX->clear();
            rechitClusterSizeY->clear();
            rechitCharge->clear();
            rechitTime->clear();
        }

        /**
         * Branch variables:
         */
        int orbitNumber, bunchCounter, eventCounter;

        /** 
         * Cluster variables:
         * a cluster of strips is defined by chamber, eta, center, size
         * a cluster of pads is defined by chamber, center x, center y, size
         */
        std::vector<int> *clusterStripChamber;
        std::vector<int> *clusterStripEta;
        std::vector<double> *clusterStripCenter;
        std::vector<double> *clusterStripSize;
        std::vector<int> *clusterPadChamber;
        std::vector<double> *clusterPadCenterX;
        std::vector<double> *clusterPadCenterY;
        std::vector<double> *clusterPadSizeX;
        std::vector<double> *clusterPadSizeY;

        /**
         * Rechit variables:
         * a rechit is defined by chamber, eta, x, y, error x, error y, cluster size
         * for a rechit built by pads, eta is always 1
         */
        std::vector<int> *rechitChamber;
        std::vector<int> *rechitEta;
        std::vector<double> *rechitX;
        std::vector<double> *rechitY;
        std::vector<double> *rechitErrorX;
        std::vector<double> *rechitErrorY;
        std::vector<double> *rechitR;
        std::vector<double> *rechitPhi;
        std::vector<double> *rechitClusterSize;
        std::vector<double> *rechitClusterSizeX;
        std::vector<double> *rechitClusterSizeY;
        std::vector<int> *rechitCharge;
        std::vector<double> *rechitTime;

        void print() {
            fmt::print("Rechit event:\n");
            fmt::print(
                    "  EC {}, OC {}, BC {}\n",
                    eventCounter, orbitNumber, bunchCounter
                    );
            fmt::print("  Strip clusters:\n");
            fmt::print(
                    "    {:>7}\t{:>7}\t{:>7}\t{:>7}\n",
                    "chamber", "eta", "center", "size"
                    );
            for (int i=0; i<clusterStripChamber->size(); i++) {
                fmt::print(
                        "    {:>7}\t{:>7}\t{:>7.2f}\t{:>7.2f}\n",
                        clusterStripChamber->at(i),
                        clusterStripEta->at(i),
                        clusterStripCenter->at(i),
                        clusterStripSize->at(i)
                        );
            }
            fmt::print("  Pad clusters:\n");
            fmt::print(
                    "    {:>7}\t{:>9}\t{:>9}\t{:>7}\t{:>7}\n",
                    "chamber", "center x", "center y", "size x", "size y"
                    );
            for (int i=0; i<clusterPadChamber->size(); i++) {
                fmt::print(
                        "    {:>7}\t{:>9.2f}\t{:>9.2f}\t{:>7.2f}\t{:>7.2f}\n",
                        clusterPadChamber->at(i),
                        clusterPadCenterX->at(i),
                        clusterPadCenterY->at(i),
                        clusterPadSizeX->at(i),
                        clusterPadSizeY->at(i)
                        );
            }
            fmt::print("  Rechits:\n");
            fmt::print(
                    "    {:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\n",
                    "chamber", "eta", "x", "y", "err x", "err y", "r", "phi", "size", "size x", "size y", "charge", "time"
                    );
            for (int i=0; i<rechitChamber->size(); i++) {
                fmt::print(
                        "    {:>7d}\t{:>7d}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7.2f}\t{:>7d}\t{:>7.2f}\n",
                        rechitChamber->at(i),
                        rechitEta->at(i),
                        rechitX->at(i),
                        rechitY->at(i),
                        rechitErrorX->at(i),
                        rechitErrorY->at(i),
                        rechitR->at(i),
                        rechitPhi->at(i),
                        rechitClusterSize->at(i),
                        rechitClusterSizeX->at(i),
                        rechitClusterSizeY->at(i),
                        rechitCharge->at(i),
                        rechitTime->at(i)
                        );
            }
            fmt::print("-----------------------------------------------------\n");

        }
};

#endif
