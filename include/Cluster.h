#include <vector>

#include "Digi.h"

#ifndef DEF_CLUSTER
#define DEF_CLUSTER

class Cluster {

    public:

        Cluster(int chamber) : m_chamber{chamber} {}

        int getChamber() { return m_chamber; }
        int getEta() { return m_eta; }

        std::ostream& operator<<(std::ostream &os) {
            return os << "chamber " << m_chamber;
        }

    protected:

        int m_chamber;
        int m_eta = 1;
};

#endif
