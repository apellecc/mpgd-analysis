#include <cstdio>
#include <vector>
#include <map>
#include <memory>

#include "DetectorGeometry.h"
#include "DetectorLayer.h"
#include "TrackingFactory.h"

#ifndef DEF_SETUP_GEOMETRY
#define DEF_SETUP_GEOMETRY

class TrackingAlgorithm; // forward declaration to avoid circular dependency 

class SetupGeometry {
    public:

        SetupGeometry(std::string geometryFile);

        void print();
        DetectorGeometry *getChamber(int chamber);
        int getLayer(int chamber);
        bool isTrackingChamber(int chamber);
        bool isTrackingLayer(int layer);
        
        std::shared_ptr<TrackingAlgorithm> getTrackingAlgorithm();

        /**
         * List of tracking chamber IDs:
         */
        std::vector<int> trackerChambers;
        /**
         * Map from chamber number to detector:
         */
        std::map<int, DetectorGeometry> detectorMap;
        /**
         * List of detectors with the same z position:
         */
        std::map<double, std::vector<int>> layerMap;
        /**
         * Map each detector to corresponding layer:
         */
        std::map<int, int> detectorToLayerMap;
        /**
         * List of layer objects:
         */
        std::vector<DetectorLayer> layers;

    private:

        /**
         * @brief Algorithm to be used for track building, 
         * created by TrackingFactory based on geometry file configuration
         */
        std::shared_ptr<TrackingAlgorithm> m_trackingAlgorithm;
};

#endif

