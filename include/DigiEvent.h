#include <fmt/core.h>

#ifndef DEF_DIGI_EVENT
#define DEF_DIGI_EVENT

class DigiEvent {
    
    public:

        /**
         * Empty constructor
         */
        DigiEvent () {
            rawFED = new std::vector<int>();
            rawSlot = new std::vector<int>();
            rawOH = new std::vector<int>();
            rawVFAT = new std::vector<int>();
            rawChannel = new std::vector<int>();
            digiStripChamber = new std::vector<int>();
            digiStripEta = new std::vector<int>();
            digiStrip = new std::vector<int>();
            digiStripCharge = new std::vector<short>();
            digiStripTime = new std::vector<int>();
            digiPadChamber = new std::vector<int>();
            digiPadX = new std::vector<int>();
            digiPadY = new std::vector<int>();
            digiPadCharge = new std::vector<short>();
            digiPadTime = new std::vector<int>();
        }

        ~DigiEvent() {
            clear();
        }

        void clear() {
            subEventCount = 0;
            rawFED->clear();
            rawSlot->clear();
            rawOH->clear();
            rawVFAT->clear();
            rawChannel->clear();
            digiStripChamber->clear();
            digiStripEta->clear();
            digiStrip->clear();
            digiStripCharge->clear();
            digiStripTime->clear();
            digiPadChamber->clear();
            digiPadX->clear();
            digiPadY->clear();
            digiPadCharge->clear();
            digiPadTime->clear();
        }

        /**
         * Branch variables:
         */
        int runParameter{0}, pulseStretch{0};
        int orbitNumber, bunchCounter, eventCounter;
        // for multi-BX readout
        int subEventCount{0};

        // raw variables
        std::vector<int> *rawFED;
        std::vector<int> *rawSlot;
        std::vector<int> *rawOH;
        std::vector<int> *rawVFAT;
        std::vector<int> *rawChannel;
        // digi variables for strip and pad detectors
        std::vector<int> *digiStripChamber;
        std::vector<int> *digiStripEta;
        std::vector<int> *digiStrip;
        std::vector<int> *digiPadChamber;
        std::vector<int> *digiPadX;
        std::vector<int> *digiPadY;
        /**
         * Peak charge of the signal 
         */
        std::vector<short> *digiStripCharge;
        std::vector<short> *digiPadCharge;
        /**
         * Time at the signal peak
         */
        std::vector<int> *digiStripTime;
        std::vector<int> *digiPadTime;

        void print() {
            std::cout << "Digi event:" << std::endl;
            fmt::print("    Latency {}, pulse stretch {}, EC {}, OC {}, BC {}\n",
                    runParameter, pulseStretch, eventCounter, orbitNumber, bunchCounter
                    );
            fmt::print("    {:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\n", "chamber", "eta", "strip", "charge", "time");
            for (int i=0; i<digiStripChamber->size(); i++) {
                fmt::print("    {:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\n",
                        digiStripChamber->at(i),
                        digiStripEta->at(i),
                        digiStrip->at(i),
                        digiStripCharge->at(i),
                        digiStripTime->at(i)
                        );
            }
            fmt::print("    {:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\n", "chamber", "pad x", "pad y", "charge", "time");
            for (int i=0; i<digiPadChamber->size(); i++) {
                fmt::print("    {:>7}\t{:>7}\t{:>7}\t{:>7}\t{:>7}\n",
                        digiPadChamber->at(i),
                        digiPadX->at(i),
                        digiPadY->at(i),
                        digiPadCharge->at(i),
                        digiPadTime->at(i)
                        );
            }
            fmt::print("-----------------------------------------------------\n");

        }
};

#endif
