# mpgd-analysis readme

This repository contains a reconstruction and analysis software for MPGD data.
To get started with the code read the [documentation](https://mpgd-analysis.docs.cern.ch/guide/compile/).

## Supported geometries

An up-to-date list of the supported setups is available in the [geometry page](https://mpgd-analysis.docs.cern.ch/reconstruction/geometry/#supported-setups) in the documentation.

## How to contribute to the code

If you have already read the documentation, follow these steps to create your branch and open a merge request:

1. Create a branch for your modifications. Use consistent branch namings, for example:
```bash
git checkout -B feature/channel-mask-may2022
```

2. Add the files you have modified, such as:
```bash
git add masks/may2022.csv
```

3. Commit your modifications, e.g.:
```bash
git commit -m "Add channel masking for May 2022 test beam"
```

4. Push the changes, then open a merge request following the instructions on the terminal:
```bash
git push origin feature/channel-mask-may2022
```