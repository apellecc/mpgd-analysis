## How to profile an application

Prerequisite: check that `gprof`, `xdot`, `graphviz` and `gprof2dot` are installed on your system.

1. Compile the repository with the options `-DCMAKE_CXX_FLAGS=-pg -DCMAKE_EXE_LINKER_FLAGS=-pg -DCMAKE_SHARED_LINKER_FLAGS=-pg`.
2. Run the executable you need to profile.
3. Run `gprof` on the executable output:
```bash
gprof $EXECUTABLE_NAME gmon.out >> gprof_data.out
```
4. Generate a call graph file from the profiler output:
```bash
gprof2dot gprof_data.out > call_graph.dot
```

The call graph can be opened with `xdot` or converted to `png` with:
```bash
dot -Tpng call_graph.dot -o call_graph.png
```
